$(function () {
    
	
	$(document).on("click", ".wishBtn", function () { 
        popUp("#viewAllBirthdays", "close");
        popUp("#viewAllBirthdays", "close");
    });
	
   $(document).on("click", "#apply", function () {
	  
        popUp("#customizeCardPopup", "close");
        popUp("#customizeQuickLinks", "close");
    });
    
	
    $(document).on("click", "#assetCardPopup", function () {
        $('#assetCardContent').show();
        $('#userProfile').hide();
        $("body").disableScroll();
    });
    

    $(document).on("click", "#fullFinalCardPopup", function () {
        $('#fullFinalCardContent').show();
        $('#userProfile').hide();
        $("body").disableScroll();
    });
    

    $(document).on("click", "#leavesCardPopup", function () {
        $('#leavesCardContent').show();
        $('#userProfile').hide();
        $("body").disableScroll();
    });

    
    $(document).on('click', '#actionsCardPopup', function () {
        $('#actionsCardContent').show();
        $('#userProfile').hide();
        $("body").disableScroll();
    });

    // When the user clicks on notification div, open the popup
    $(document).on('click', '#profileCardPopup', function () {
        $('#profileCardContent').show();
        $('#userProfile').hide();
        $("body").disableScroll();
    });

    // When the user clicks on notification div, open the popup
    $(document).on('click', '#quickCardPopup', function () {
        $('#quickCardContent').show();
        $('#userProfile').hide();
        $("body").disableScroll();
    });



    // Initialize popup
    $("#customizeCardPopup").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true
    });

    $("#birthdayWishes").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true
    });
    $("#sendWish").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true
    });
    $("#viewAllBirthdays").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true
    });
    $("#viewAllHolidays").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true
    });
    $("#viewAllAnniversaries").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true
    });
    $("#customizeQuickLinks").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true
    });

    // Function for closing popup
    function popUp(element, action) {
        $(element).dialog(action);
        return false;
    }

    // Open popup on click
    $(document).on("click", "#addMoreCards", function () {
        popUp("#customizeCardPopup", "open");
        $("#customizeCardPopup").css({
            visibility: "visible"
        });
    });
    // Open popup on click
    // $(document).on("click", "#wish", function () {
    //     popUp("#birthdayWishes", "open");
    //     $("#birthdayWishes").css({
    //         visibility: "visible"
    //     });
    // });
    // Open popup on click
    $(document).on("click", "#wish", function () {
        popUp("#sendWish", "open");
        $("#sendWish").css({
            visibility: "visible"
        });
    });
    // Open popup on click
    $(document).on("click", "#viewBirthdays", function () {
        popUp("#viewAllBirthdays", "open");
        $("#viewAllBirthdays").css({
            visibility: "visible"
        });
    });
    // Open popup on click
    $(document).on("click", "#viewHolidays", function () {
        popUp("#viewAllHolidays", "open");
        $("#viewAllHolidays").css({
            visibility: "visible"
        });
    });
    $(document).on("click", "#viewAnniversaries", function () {
        popUp("#viewAllAnniversaries", "open");
        $("#viewAllAnniversaries").css({
            visibility: "visible"
        });
    });
    $(document).on("click", "#customizeQuickLinksOpen", function () {
        popUp("#customizeQuickLinks", "open");
        $("#customizeQuickLinks").css({
            visibility: "visible"
        });
    });

    // Close popup on click
    $("#closePopup, #closePopupBtn, #applyCardsOrder").on("click", function () {
        popUp("#customizeCardPopup", "close");
        popUp("#birthdayWishes", "close");
        popUp("#sendWish", "close");
        popUp("#viewAllBirthdays", "close");
        popUp("#viewAllHolidays", "close");
        popUp("#viewAllAnniversaries", "close");
        popUp("#customizeQuickLinks", "close");
    });

    // Select cards
    $(".sortCards li").click(function () {
        let liArray = $(this).children()[1].classList;
        if (liArray.contains("selectedCard")) {
            $(this)
                .children()[1]
                .classList.remove("selectedCard");
            $(this).children()[1].innerHTML = "check_circle_outline";
            return;
        }
        $(this)
            .children()[1]
            .classList.add("selectedCard");
        $(this).children()[1].innerHTML = "check_circle";
    });


    let mainAssetCardHead = "<div class='cardHeader'> <h4>Assets</h4> <a href='#' class='togglePopup' id='assetCardPopup'> <i class='material-icons'> more_vert</i> </a> <div class='popup MenuCardPopup' id='assetCardContent'> <ul class='dashboardMenuPopup'> <li> <a href='#'><img src='../dist/images/attendanceBlack.png' alt='attendance'>View Assets</a> </li> <li> <a href='#'><img src='../dist/images/historyBlack.png' alt='history'>View History</a> </li> </ul> </div> </div>";
    let mainAssetCardContent = "<div class='cardContent'> <h2>13<span>Pending Approval</span> </h2> <button class='oct-btn oct-btn-primary'>View Requests</button> </div>";
    let mainFullFinalCardHead = "<div class='cardHeader'> <h4>Full & Final Settlements</h4> <a href='#' class='togglePopup' id='fullFinalCardPopup'> <i class='material-icons'> more_vert</i> </a> <div class='popup MenuCardPopup' id='fullFinalCardContent'> <ul class='dashboardMenuPopup'> <li> <a href='#'><img src='../dist/images/attendanceBlack.png' alt='attendance'>View F&F</a> </li> <li> <a href='#'><img src='../dist/images/historyBlack.png' alt='history'>View History</a> </li> </ul> </div> </div>";
    let mainFullFinalCardContent = "<div class='cardContent'> <h2>13<span>Pending Approval</span> </h2> <button class='oct-btn oct-btn-primary'>Settle F&F</button> </div>";
    let mainLeavesCardHead = "<div class='cardHeader'> <h4>Leaves</h4> <a href='#' class='togglePopup' id='leavesCardPopup'> <i class='material-icons'> more_vert</i> </a> <div class='popup MenuCardPopup' id='leavesCardContent'> <ul class='dashboardMenuPopup'> <li> <a href='#'><img src='../dist/images/attendanceBlack.png' alt='attendance'>View Leaves</a> </li> <li> <a href='#'><img src='../dist/images/historyBlack.png' alt='history'>View History</a> </li> </ul> </div> </div>";
    let mainLeavesCardContent = "<div class='cardContent'> <h2>2<span>Pending Approval</span> </h2> <button class='oct-btn oct-btn-primary mr-3'>View Requests</button> <button class='oct-btn oct-btn-primary'>Apply on Behalf</button> </div>";

    let quickLinkCardHead = "<div class='cardHeader'> <h4>Quick Links</h4> <a href='#' class='togglePopup' id='quickCardPopup'><i class='material-icons'>more_vert</i></a> <div class='popup MenuCardPopup' id='quickCardContent'> <ul class='dashboardMenuPopup'> <li><a href='#'><img src='../dist/images/custumizeBlack.png' alt='Profile'>Customize</a></li>  </ul> </div> </div>";

    let quickLinkCardContent = '<div class="cardContent"> <div class="row"> <div class="col-md-6 col-sm-12 col-xs-12"> <img src="../dist/images/file.png" alt="file"> <p>Performance and Review</p> </div> <div class="col-md-6 col-sm-12 col-xs-12"><img src="../dist/images/note.png" alt="note"> <p>Referrals and Oppurtunuties</p> </div> </div> <div class="row mt-4"> <div class="col-md-6 col-sm-12 col-xs-12"> <img src="../dist/images/note.png" alt="note"> <p>View Pay Slip</p> </div> <div class="col-md-6 col-sm-12 col-xs-12"><img src="../dist/images/file.png" alt="file"> <p>View form 16</p> </div> </div> </div>';

    let myActionsCardHead = "<div class='cardHeader'> <h4>My Actions</h4> <a href='#' class='togglePopup' id='actionsCardPopup'><i class='material-icons'> more_vert</i></a> <div class='popup MenuCardPopup' id='actionsCardContent'> <ul class='dashboardMenuPopup'> <li><a href='#'><img src='../dist/images/attendanceBlack.png' alt='attendance'>View My Actions</a> </li> <li><a href='#'><img src='../dist/images/historyBlack.png' alt='history'>View Activities</a></li>  </ul> </div> </div>";

    let myActionsCardContent = '<div class="cardContent"> <ul> <li>Submit investment proof.</li> <li>Interview feedback submission.</li> <li>Health check-up camp in cafetaria at 3pm</li> <li><a href="#">Riya Sharma</a> requested the course Angular Development scheduled on Mar 18th.</li> </ul> </div>';

    let masterCardHead = "<div class='cardHeader'> <select id='contentTypeOptions'> <option value='0'>Asset Master</option> <option value='1'>Employee Master</option> <option value='2'>Candidate Master</option> </select></div>"

    let masterCardContent = "<div class='cardContent statsContent'> <div id='assetMaster'> <div class='masterDate'> <span class='masterType'>Accessories</span> <span class='masterTotal'>19</span> </div> <div class='masterDate'> <span class='masterType'>Parts</span> <span class='masterTotal'>21</span> </div> <div class='masterDate'> <span class='masterType'>Laptop/Computers</span> <span class='masterTotal'>18</span> </div> <div class='masterDate'> <span class='masterType'>Mobile</span> <span class='masterTotal'>18</span> </div> <div class='cardContentFooter'> <hr class='no-padding'> <a href='#'>View Assets</a> </div> </div> <div id='employeeMaster'> <div class='masterDate'> <span class='masterType'>Regular on Roll</span> <span class='masterTotal'>238</span> </div> <div class='masterDate'> <span class='masterType'>Worker</span> <span class='masterTotal'>45</span> </div> <div class='masterDate'> <span class='masterType'>Fixed Term Employment</span> <span class='masterTotal'>63</span> </div> <div class='masterDate'> <span class='masterType'>Resident Engineer</span> <span class='masterTotal'>2</span> </div> <div class='cardContentFooter'> <hr class='no-padding'> <a href='#'>View Employees</a> </div> </div> <div id='candidateMaster'> <div class='masterDate'> <span class='masterType'>Applied</span> <span class='masterTotal'>19</span> </div> <div class='masterDate'> <span class='masterType'>Shortlisted</span> <span class='masterTotal'>21</span> </div> <div class='masterDate'> <span class='masterType'>Interview In process</span> <span class='masterTotal'>18</span> </div> <div class='masterDate'> <span class='masterType'>Selected</span> <span class='masterTotal'>14</span> </div> <div class='masterDate'> <span class='masterType'>Rejected</span> <span class='masterTotal'>3</span> </div> <div class='masterDate'> <span class='masterType'>Offered</span> <span class='masterTotal'>16</span> </div> <div class='cardContentFooter'> <hr class='no-padding'> <a href='#'>View Candidate</a> </div> </div> </div>";

    function camelize(str) {
        return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
            return index == 0 ? word.toLowerCase() : word.toUpperCase();
        }).replace(/\s+/g, '');
    }

    function cardHTMLTop(text) {
        let headHTML = "";
        let contentHTML = "";
        let currentCard = text.split(" ").join("-");

        switch (currentCard) {
            case "main-asset-card":
                headHTML = mainAssetCardHead;
                contentHTML = mainAssetCardContent;
                break;
            case "main-full-final-card":
                headHTML = mainFullFinalCardHead;
                contentHTML = mainFullFinalCardContent;
                break;
            case "main-leaves-card":
                headHTML = mainLeavesCardHead;
                contentHTML = mainLeavesCardContent;
                break;
            default:
                break;
        }
        return (
            '<div class="col dashboardCard" id=' + camelize(text) + ' data-card=' + text.split(" ").join("-") + '>' + headHTML + contentHTML + '</div>'
        );
    }

    function cardHTMLMain(text) {
        let headHTML = "";
        let contentHTML = "";
        let currentCard = text.split(" ").join("-");

        switch (currentCard) {
            case "my-actions":
                headHTML = myActionsCardHead;
                contentHTML = myActionsCardContent;
                break;
            case "quick-links":
                headHTML = quickLinkCardHead;
                contentHTML = quickLinkCardContent;
                break;
            case "master":
                headHTML = masterCardHead;
                contentHTML = masterCardContent;
                break;
            default:
                break;
        }
        return (
            '<div class="col col-md-4 cards" id=' + camelize(text) + ' data-card=' + text.split(" ").join("-") + '>' + headHTML + contentHTML + '</div>'
        );
    }
    // Make cards sortable
    $(".connectedSortableMain")
        .sortable({
            connectWith: ".connectedSortableMain",
            placeholder: "ui-state-highlight"
        })
        .disableSelection();

    $(".connectedSortableTop")
        .sortable({
            connectWith: ".connectedSortableTop",
            placeholder: "ui-state-highlight"
        })
        .disableSelection();

    $("#applyCardsOrder").click(function () {
        sortAndDisplay(".connectedSortableTop li", "topRow");
        sortAndDisplay(".connectedSortableMain li", "mainRow");
    });

    function getCardList(el) {
        return $(el)
        .toArray()
        .filter(function (i) {
            return i.children[1].classList.contains("selectedCard");
        });
    }

    function getCardTypeList(el) {
        return el.map(function (i) {
            return i.attributes[0];
        })
        .map(function (i) {
            return i.nodeValue;
        });
    }


    function sortAndDisplay(list, container) {
        let cardsOrder = [];
        let cardsHtml = "";
    
        let liArray = getCardList(list);

        let cardTypeList = getCardTypeList(liArray);
            

        cardsOrder = cardTypeList.map(function (card) {
            return card.split("-").join(" ");
        });

        cardsHtml = cardsOrder.map(function (cardName) {
            switch(container) {
                case "topRow": 
                    return cardHTMLTop(cardName);
                case "mainRow": 
                    return cardHTMLMain(cardName);
                default: break;
            }
        })
        document.getElementById(container).innerHTML = '';
        document.getElementById(container).innerHTML = cardsHtml.join("");
    }


    $('.carousel').carousel({
        interval: false
    })

    // Confetti Code 

    var COLORS, Confetti, NUM_CONFETTI, PI_2, canvas, confetti, context, drawCircle, drawCircle2, drawCircle3, i, range, xpos;
    NUM_CONFETTI = 100;
    COLORS = [
        [235, 90, 70],
        [97, 189, 79],
        [242, 214, 0],
        [0, 121, 191],
        [195, 119, 224]
    ];
    PI_2 = 2 * Math.PI;
    canvas = document.getElementById("confeti");
    context = canvas.getContext("2d");
    window.w = 0;
    window.h = 0;
    window.resizeWindow = function () {
        window.w = canvas.width = window.innerWidth;
        return window.h = canvas.height = window.innerHeight
    };
    window.addEventListener("resize", resizeWindow, !1);
    window.onload = function () {
        return setTimeout(resizeWindow, 0)
    };
    range = function (a, b) {
        return (b - a) * Math.random() + a
    };
    drawCircle = function (a, b, c, d) {
        context.beginPath();
        context.moveTo(a, b);
        context.bezierCurveTo(a - 17, b + 14, a + 13, b + 5, a - 5, b + 22);
        context.lineWidth = 2;
        context.strokeStyle = d;
        return context.stroke()
    };
    drawCircle2 = function (a, b, c, d) {
        context.beginPath();
        context.moveTo(a, b);
        context.lineTo(a + 6, b + 9);
        context.lineTo(a + 12, b);
        context.lineTo(a + 6, b - 9);
        context.closePath();
        context.fillStyle = d;
        return context.fill()
    };
    drawCircle3 = function (a, b, c, d) {
        context.beginPath();
        context.moveTo(a, b);
        context.lineTo(a + 5, b + 5);
        context.lineTo(a + 10, b);
        context.lineTo(a + 5, b - 5);
        context.closePath();
        context.fillStyle = d;
        return context.fill()
    };
    xpos = 0.9;
    document.onmousemove = function (a) {
        return xpos = a.pageX / w
    };
    window.requestAnimationFrame = function () {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (a) {
            return window.setTimeout(a, 5)
        }
    }();
    Confetti = function () {
        function a() {
            this.style = COLORS[~~range(0, 5)];
            this.rgb = "rgba(" + this.style[0] + "," + this.style[1] + "," + this.style[2];
            this.r = ~~range(2, 6);
            this.r2 = 2 * this.r;
            this.replace()
        }
        a.prototype.replace = function () {
            this.opacity = 0;
            this.dop = 0.03 * range(1, 4);
            this.x = range(-this.r2, w - this.r2);
            this.y = range(-20, h - this.r2);
            this.xmax = w - this.r;
            this.ymax = h - this.r;
            this.vx = range(0, 2) + 8 * xpos - 5;
            return this.vy = 0.7 * this.r + range(-1, 1)
        };
        a.prototype.draw = function () {
            var a;
            this.x += this.vx;
            this.y += this.vy;
            this.opacity +=
                this.dop;
            1 < this.opacity && (this.opacity = 1, this.dop *= -1);
            (0 > this.opacity || this.y > this.ymax) && this.replace();
            if (!(0 < (a = this.x) && a < this.xmax)) this.x = (this.x + this.xmax) % this.xmax;
            drawCircle(~~this.x, ~~this.y, this.r, this.rgb + "," + this.opacity + ")");
            drawCircle3(0.5 * ~~this.x, ~~this.y, this.r, this.rgb + "," + this.opacity + ")");
            return drawCircle2(1.5 * ~~this.x, 1.5 * ~~this.y, this.r, this.rgb + "," + this.opacity + ")")
        };
        return a
    }();
    confetti = function () {
        var a, b, c;
        c = [];
        i = a = 1;
        for (b = NUM_CONFETTI; 1 <= b ? a <= b : a >= b; i = 1 <= b ? ++a : --a) c.push(new Confetti);
        return c
    }();
    window.step = function () {
        var a, b, c, d;
        requestAnimationFrame(step);
        context.clearRect(0, 0, w, h);
        d = [];
        b = 0;
        for (c = confetti.length; b < c; b++) a = confetti[b], d.push(a.draw());
        return d
    };
    step();


});

$(document).on("change", "#contentTypeOptions", function () {
    const selectedOption = $(this)[0].options.selectedIndex;

    // Get selected HTML content
    const content = $('.statsContent').children().toArray()[selectedOption];
    // Display it
    content.style.display = 'flex';

    // Get selected HTML content's id
    const selectedOptionId = content.attributes[0].value;
    // Hide it's siblings
    $('.statsContent').children(`#${selectedOptionId}`).siblings().toArray().map(sibling => {
        sibling.style.display = 'none';
    });
});