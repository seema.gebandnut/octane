$("#periodofStayFrom").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#periodofStayFrom").val() != '') {
				$("#periodofStayFrom").removeClass('error');
				$('#periodofStayFrom-error').empty();
			}
                  
         }
				
});
$("#periodofStayTo").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#periodofStayTo").val() != '') {
				$("#periodofStayTo").removeClass('error');
				$('#periodofStayTo-error').empty();
			}
                  
         }	
});


$("#addrivingLicense").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#addrivingLicense").val() != '') {
				$("#addrivingLicense").removeClass('error');
				$('#addrivingLicense-error').empty();
			}
                  
         }
				
});
//show datepicker on cal icon
$(".caldl").click(function () {

	$('#addrivingLicense').datepicker("show");

});


$("#passissueDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#passissueDate").val() != '') {
				$("#passissueDate").removeClass('error');
				$('#passissueDate-error').empty();
			}
                  
         }
				
});
//show datepicker on cal icon
$(".calpass").click(function () {

	$('#passissueDate').datepicker("show");

});

$("#passportExpiry").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#passportExpiry").val() != '') {
				$("#passportExpiry").removeClass('error');
				$('#passportExpiry-error').empty();
			}
                  
         }
				
});
//show datepicker on cal icon
$(".calexp").click(function () {

	$('#passportExpiry').datepicker("show");

});


$("#adinitialJoiningDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#adinitialJoiningDate").val() != '') {
				$("#adinitialJoiningDate").removeClass('error');
				$('#adinitialJoiningDate-error').empty();
			}
                  
         }
				
});
//show datepicker on cal icon
$(".calint").click(function () {

	$('#adinitialJoiningDate').datepicker("show");

});




$("#adlastWorkingDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#adlastWorkingDate").val() != '') {
				$("#adlastWorkingDate").removeClass('error');
				$('#adlastWorkingDate-error').empty();
			}
                  
         }
				
});
//show datepicker on cal icon
$(".callast").click(function () {

	$('#adlastWorkingDate').datepicker("show");

});



$("#resgAcceptanceDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#resgAcceptanceDate").val() != '') {
				$("#resgAcceptanceDate").removeClass('error');
				$('#resgAcceptanceDate-error').empty();
			}
                  
         }
				
});
//show datepicker on cal icon
$(".calreg").click(function () {

	$('#resgAcceptanceDate').datepicker("show");

});


$("#adminconfirmationDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#adminconfirmationDate").val() != '') {
				$("#adminconfirmationDate").removeClass('error');
				$('#adminconfirmationDate-error').empty();
			}
                  
         }
				
});
//show datepicker on cal icon
$(".calcon").click(function () {

	$('#adminconfirmationDate').datepicker("show");

});
$("#adminstartDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#adminstartDate").val() != '') {
				$("#adminstartDate").removeClass('error');
				$('#adminstartDate-error').empty();
			}
                  
         }
				
});
//show datepicker on cal icon
$(".caladm").click(function () {

	$('#adminstartDate').datepicker("show");

});
$("#sepresignationDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#sepresignationDate").val() != '') {
				$("#sepresignationDate").removeClass('error');
				$('#sepresignationDate-error').empty();
			}
                  
         }
				
});

$("#prefferedDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#prefferedDate").val() != '') {
				$("#prefferedDate").removeClass('error');
				$('#prefferedDate-error').empty();
			}
                  
         }
				
});

$("#referenceDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#referenceDate").val() != '') {
				$("#referenceDate").removeClass('error');
				$('#referenceDate-error').empty();
			}
                  
         }
				
});

$("#fromlegDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#fromlegDate").val() != '') {
				$("#fromlegDate").removeClass('error');
				$('#fromlegDate-error').empty();
			}
                  
         }
				
});

$("#tolegDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#tolegDate").val() != '') {
				$("#tolegDate").removeClass('error');
				$('#tolegDate-error').empty();
			}
                  
         }
				
});

//show datepicker on cal icon
$(".calleg1").click(function () {

	$('#fromlegDate').datepicker("show");

});

//show datepicker on cal icon
$(".calsep").click(function () {

	$('#sepresignationDate').datepicker("show");

});

//show datepicker on cal icon
$(".calpref").click(function () {

	$('#prefferedDate').datepicker("show");

});

//show datepicker on cal icon
$(".calleg1").click(function () {

	$('#fromlegDate').datepicker("show");

});

//show datepicker on cal icon
$(".calref").click(function () {

	$('#referenceDate').datepicker("show");

});


$("#marriageDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#marriageDate").val() != '') {
				$("#marriageDate").removeClass('error');
				$('#marriageDate-error').empty();
			}
                  
         }
				
});

/* Updated on 5/29/2019 by Seema */
$("#dob,#dobf").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		yearRange: '1970:2019', 
		 onSelect: function () {
			if ($("#dob").val() != '') {
				$("#dob").removeClass('error');
				$('#dob-error').empty();
			}
                  
         }
				
});

$("#dobf").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		yearRange: '1970:2019', 
		 onSelect: function () {
			if ($("#dobf").val() != '') {
				$("#dobf").removeClass('error');
				$('#dobf-error').empty();
			}
                  
         }
				
});

$("#fromDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#fromDate").val() != '') {
				$("#fromDate").removeClass('error');
				$('#fromDate-error').empty();
			}
                  
         }
				
});

//show datepicker on cal icon
$(".cal1").click(function () {

	$('#fromDate').datepicker("show");

});


$("#toDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#toDate").val() != '') {
				$("#toDate").removeClass('error');
				$('#toDate-error').empty();
			}
                  
         }
				
});

//show datepicker on cal icon
$(".cal2").click(function () {

	$('#toDate').datepicker("show");

});

//show datepicker on cal icon
$(".cal9").click(function () {

	$('#dobf').datepicker("show");

});


$("#resto").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#resto").val() != '') {
				$("#resto").removeClass('error');
				$('#resto-error').empty();
			}
                  
         }
				
});
//show datepicker on cal icon
$(".cal4").click(function () {

	$('#resto').datepicker("show");

});

$("#resfrom").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#resfrom").val() != '') {
				$("#resfrom").removeClass('error');
				$('#resfrom-error').empty();
			}
                  
         }
				
});

//show datepicker on cal icon
$(".cal3").click(function () {

	$('#resfrom').datepicker("show");

});
$("#from").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#from").val() != '') {
				$("#from").removeClass('error');
				$('#from-error').empty();
			}
                  
         }
				
});

//show datepicker on cal icon
$(".cal5").click(function () {

	$('#from').datepicker("show");

});


$("#to").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#to").val() != '') {
				$("#to").removeClass('error');
				$('#to-error').empty();
			}
                  
         }
				
});

//show datepicker on cal icon
$(".cal6").click(function () {

	$('#to').datepicker("show");

});


$("#toHistory").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#toHistory").val() != '') {
				$("#toHistory").removeClass('error');
				$('#toHistory-error').empty();
			}
                  
         }
				
});

//show datepicker on cal icon
$(".cal8").click(function () {

	$('#toHistory').datepicker("show");

});



$("#fromHistory").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#fromHistory").val() != '') {
				$("#fromHistory").removeClass('error');
				$('#fromHistory-error').empty();
			}
                  
         }
				
});

//show datepicker on cal icon
$(".cal7").click(function () {

	$('#fromHistory').datepicker("show");

});

$("#checkUpDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#checkUpDate").val() != '') {
				$("#checkUpDate").removeClass('error');
				$('#checkUpDate-error').empty();
			}
                  
         }
				
});

//show datepicker on cal icon
$(".cal9").click(function () {

	$('#checkUpDate').datepicker("show");

});


$("#resignationDate").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#resignationDate").val() != '') {
				$("#resignationDate").removeClass('error');
				$('#resignationDate-error').empty();
			}
                  
         }
				
});

//show datepicker on cal icon
$(".cal10").click(function () {

	$('#resignationDate').datepicker("show");

});


$("#lastWorking").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#lastWorking").val() != '') {
				$("#lastWorking").removeClass('error');
				$('#lastWorking-error').empty();
			}
                  
         }
				
});

//show datepicker on cal icon
$(".cal12").click(function () {

	$('#proposedLastWorking').datepicker("show");

});


$("#proposedLastWorking").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		 onSelect: function () {
			if ($("#proposedLastWorking").val() != '') {
				$("#proposedLastWorking").removeClass('error');
				$('#proposedLastWorking-error').empty();
			}
                  
         }
				
});

//show datepicker on cal icon
$(".cal11").click(function () {

	$('#lastWorking').datepicker("show");

});



//show datepicker on cal icon
$(".cal").click(function () {

	$('#dob').datepicker("show");

});

/* Updated on 6/1/2019 by Seema */
function save(divID){
		
	var retvalue = formValidations(divID);
	if(retvalue==true){	
		if(divID == 'education'){
			$('#'+divID+' select').attr('disabled','disabled');
			$('#'+divID+' input').attr('disabled','disabled');
			$('#'+divID+' textarea').attr('disabled','disabled');
				var fromDate = $('#fromDate').val();
				fromDate = fromDate.toString();
				var fyear = fromDate.substr(6,4);;
				var toDate = $('#toDate').val();
				toDate = toDate.toString();
				var tyear = toDate.substr(6,4);
			var examination = $('#examination').val();
			var institute = $('#institute').val();
			var grade = $('#grade1').val();
			$('#educationTbl').append('<tr><td>'+fyear+'</td><td>'+tyear+'</td><td>'+examination+'</td><td>'+institute+'</td><td>'+grade+'</td><td class="thCenter"> <a href="#uploadPopup" rel="modal:open"><button class="oct-btn oct-btn-outline"><i class="material-icons">attachment</i></button></a><button class="oct-btn oct-btn-outline"><i class="material-icons">create</i></button><button class="oct-btn oct-btn-outline"><i class="material-icons">delete</i></button></td></tr>');
		}
	}
	
}

/* Updated on 5/29/2019 by Seema */
function updatefields(divID){
	$('#'+divID+' select').removeAttr('disabled');
	$('#'+divID+' input').removeAttr('disabled');
	$('#'+divID+' textarea').removeAttr('disabled');
	$('#'+divID+' .save').show();
	$('#'+divID+' .edit').hide();
	
}


function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();

		reader.onload = function (e) {
			$('#image_upload_preview').attr('src', e.target.result);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#inputFile").change(function () {
	readURL(this);
});


// Radio button show hide div on click
document.getElementById('opt1f').addEventListener('click', function opt1f() {

	document.getElementById('hide225').style.display = 'block';
});
document.getElementById('opt41f').addEventListener('click', function opt41f() {
	document.getElementById('hide225').style.display = 'none';
});