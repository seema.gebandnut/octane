﻿function onCheck() {
    let checkboxLen = $(".case").find('input[type="checkbox"]:checked').length;
    if (checkboxLen == 0) {
        $('#approve,#reject').attr('disabled', 'disabled');
    }
    if (checkboxLen >= 1) {
        $('#approve,#reject').removeAttr('disabled');
    }
}

function configureDatePicker(CntrlID, CntrlID_Altrnt) {
    $(CntrlID).datepicker({
        dateFormat: "dd/mm/yy",
        altField: CntrlID_Altrnt,
        altFormat: "yymmdd",
        selectOtherMonths: true
    });
}
function loadPaging() {
    $('table.paginated').each(function () {
        var currentPage = 0;
        var numPerPage = 10;
        var $table = $(this);
        $table.bind('repaginate', function () {
            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr').length;
        var numPages = Math.ceil(numRows / numPerPage);
        var $pager = $('<div class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function (event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertAfter('.actionBtns').find('span.page-number:first').addClass('active');
    });
}
function ActiveTab() {
    //var tab = $('.tab'),
    //    index = $(this).closest('li').index();

    //$('ul.tabs > li').removeClass('current');
    //$(this).closest('li').addClass('current');

    //tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp(
    //  0);
    //tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown(0);

}