﻿var APIKey = '';
$.ajax({
    type: 'POST',
    url: '/_WebServices/1_InternalServices.asmx/getGoogleMapAPIKey',
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    async: false,
    success: function (data) {
        APIKey = data.d;
    },
    error: function (xhr) { }
});
$("head").append('<script type="text/javascript" src="https://maps.google.com/maps/api/js?key='+APIKey+'"></script>');