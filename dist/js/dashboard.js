$(function () {

  $(document).on("click", ".wishBtn", function () { 
        popUp("#viewAllBirthdays", "close");
        popUp("#viewAllBirthdays", "close");
    });

	  $(document).on("click", "#apply", function () { 
        popUp("#customizeCardPopup", "close");
        popUp("#customizeQuickLinks", "close");
    });
    
    // replaced next line
    $(document).on("click", "#attendanceCardPopup", function () {
        $('#attendanceCardContent').show();
        $('#userProfile').hide();
        $("body").disableScroll();
    });

    // replaced next line
    $(document).on("click", "#leavesCardPopup", function () {
        $('#leavesCardContent').show();
        $('#userProfile').hide();
        $("body").disableScroll();
    });

    // replaced next line
    $(document).on("click", "#reimbursementCardPopup", function () {
        $('#reimbursementCardContent').show();
        $('#userProfile').hide();
        $("body").disableScroll();
    });

    // When the user clicks on notification div, open the popup
    $(document).on('click', '#actionsCardPopup', function () {
        $('#actionsCardContent').show();
        $('#userProfile').hide();
        $("body").disableScroll();
    });

    // When the user clicks on notification div, open the popup
    $(document).on('click', '#profileCardPopup', function () {
        $('#profileCardContent').show();
        $('#userProfile').hide();
        $("body").disableScroll();
    });

    // When the user clicks on notification div, open the popup
    $(document).on('click', '#quickCardPopup', function () {
        $('#quickCardContent').show();
        $('#userProfile').hide();
        $("body").disableScroll();
    });



    // Initialize popup
    $("#customizeCardPopup").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true
    });

    $("#birthdayWishes").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true
    });
    $("#sendWish").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true
    });
    $("#viewAllBirthdays").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true
    });
    $("#viewAllHolidays").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true
    });
    $("#viewAllAnniversaries").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true
    });
    $("#customizeQuickLinks").dialog({
        autoOpen: false,
        modal: true,
        closeOnEscape: true
    });

    // Function for closing popup
    function popUp(element, action) {
        $(element).dialog(action);
        return false;
    }

    // Open popup on click
    $(document).on("click", "#addMoreCards", function () {
        popUp("#customizeCardPopup", "open");
        $("#customizeCardPopup").css({
            visibility: "visible"
        });
    });
    // Open popup on click
    // $(document).on("click", "#wish", function () {
    //     popUp("#birthdayWishes", "open");
    //     $("#birthdayWishes").css({
    //         visibility: "visible"
    //     });
    // });
    // Open popup on click
    $(document).on("click", "#wish", function () {
        popUp("#sendWish", "open");
        $("#sendWish").css({
            visibility: "visible"
        });
    });
    // Open popup on click
    $(document).on("click", "#viewBirthdays", function () {
        popUp("#viewAllBirthdays", "open");
        $("#viewAllBirthdays").css({
            visibility: "visible"
        });
    });
    // Open popup on click
    $(document).on("click", "#viewHolidays", function () {
        popUp("#viewAllHolidays", "open");
        $("#viewAllHolidays").css({
            visibility: "visible"
        });
    });
    $(document).on("click", "#viewAnniversaries", function () {
        popUp("#viewAllAnniversaries", "open");
        $("#viewAllAnniversaries").css({
            visibility: "visible"
        });
    });
    $(document).on("click", "#customizeQuickLinksOpen", function () {
        popUp("#customizeQuickLinks", "open");
        $("#customizeQuickLinks").css({
            visibility: "visible"
        });
    });

    // Close popup on click
    $("#closePopup, #closePopupBtn, #applyCardsOrder").on("click", function () {
        popUp("#customizeCardPopup", "close");
        popUp("#birthdayWishes", "close");
        popUp("#sendWish", "close");
        popUp("#viewAllBirthdays", "close");
        popUp("#viewAllHolidays", "close");
        popUp("#viewAllAnniversaries", "close");
        popUp("#customizeQuickLinks", "close");
    });

    // Select cards
    // changed line below
    $(".sortCards li").click(function () {
        let liArray = $(this).children()[1].classList;
        if (liArray.contains("selectedCard")) {
            $(this)
                .children()[1]
                .classList.remove("selectedCard");
            $(this).children()[1].innerHTML = "check_circle_outline";
            return;
        }
        $(this)
            .children()[1]
            .classList.add("selectedCard");
        $(this).children()[1].innerHTML = "check_circle";
    });
    //added lines 167, 167, 171, 173, 177,
    let mainAttendanceCardHead = "<div class='cardHeader'> <h4>Attendance</h4> <a href='#' class='togglePopup' id='attendanceCardPopup'><i class='material-icons'> more_vert</i></a> <div class='popup MenuCardPopup' id='attendanceCardContent'> <ul class='dashboardMenuPopup'> <li><a href='#'><img src='../dist/images/attendanceBlack.png' alt='attendance'>View Attendance</a> </li> <li><a href='#'><img src='../dist/images/historyBlack.png' alt='history'>View History</a></li> </ul> </div> </div>";

    let mainAttendanceCardContent = "<div class='cardContent'> <div class='fields'> <input type='text' name='' placeholder='Add a note'> </div> <div> <button class='oct-btn oct-btn-success'> Check-In</button> <button class='oct-btn oct-btn-danger ml-1'>Check-Out</button> <span id='displayTime'>10:29:45 am</span> </div> </div>";

    let mainLeaveCardHead = "<div class='cardHeader'> <h4>Leaves</h4> <a href='#' class='togglePopup' id='leavesCardPopup'><i class='material-icons'> more_vert</i></a> <div class='popup MenuCardPopup' id='leavesCardContent'> <ul class='dashboardMenuPopup'> <li><a href='#'><img src='../dist/images/attendanceBlack.png' alt='attendance'>View Leaves</a> </li> <li><a href='#'><img src='../dist/images/historyBlack.png' alt='history'>View History</a></li> <li><a href='#'><img src='../dist/images/summaryBlack.png' alt='summary'>Summary</a> </li> </ul> </div> </div>";

    let mainLeaveCardContent = "<div class='cardContent'> <h2>13<span>Pending Approval</span></h2> <button class='oct-btn oct-btn-primary'>Request Leave</button> </div>";

    let mainReimbursementCardHead = "<div class='cardHeader'> <h4>Reimbursement</h4> <a href='#' class='togglePopup' id='reimbursementCardPopup'><i class='material-icons'> more_vert</i></a> <div class='popup MenuCardPopup' id='reimbursementCardContent'> <ul class='dashboardMenuPopup'> <li><a href='#'><img src='../dist/images/attendanceBlack.png' alt='attendance'>View Reimbursement</a> </li> <li><a href='#'><img src='../dist/images/historyBlack.png' alt='history'>View History</a></li> </ul> </div> </div>";

    let mainReimbursementCardContent = "<div class='cardContent'> <h2>2<span>Pending Approval</span></h2> <button class='oct-btn oct-btn-primary'>Request Reimbursement</button> </div>";

    let quickLinkCardHead = "<div class='cardHeader'> <h4>Quick Links</h4> <a href='#' class='togglePopup' id='quickCardPopup'><i class='material-icons'>more_vert</i></a> <div class='popup MenuCardPopup' id='quickCardContent'> <ul class='dashboardMenuPopup'> <li><a href='#'><img src='../dist/images/custumizeBlack.png' alt='Profile'>Customize</a></li>  </ul> </div> </div>";

    let quickLinkCardContent = '<div class="cardContent"> <div class="row"> <div class="col-md-6 col-sm-12 col-xs-12"> <img src="../dist/images/file.png" alt="file"> <p>Performance and Review</p> </div> <div class="col-md-6 col-sm-12 col-xs-12"><img src="../dist/images/note.png" alt="note"> <p>Referrals and Oppurtunuties</p> </div> </div> <div class="row mt-4"> <div class="col-md-6 col-sm-12 col-xs-12"> <img src="../dist/images/note.png" alt="note"> <p>View Pay Slip</p> </div> <div class="col-md-6 col-sm-12 col-xs-12"><img src="../dist/images/file.png" alt="file"> <p>View form 16</p> </div> </div> </div>';

    let profileCardHead = "<div class='cardHeader'> <h4>Profile</h4> <a href='#' class='togglePopup' id='profileCardPopup'><i class='material-icons'> more_vert</i></a> <div class='popup MenuCardPopup' id='profileCardContent'> <ul class='dashboardMenuPopup'> <li><a href='#'><img src='../dist/images/profileBlack.png' alt='Profile'>View My Profile</a> </li> <li><a href='#'><img src='../dist/images/historyBlack.png' alt='history'>View Activities</a></li> </ul> </div> </div>";

    let profileCardContent = '<div class="cardContent"> <div class="progress"> <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"> </div> </div> <span class="progressButton">80%</span> <ul> <li><i class="material-icons">description</i>Add your summary</li> <li><i class="material-icons">stars</i>Add your primary & secondary skills</li> <li><i class="material-icons">account_circle</i>Add your profile picture</li> </ul> <a class="profileMore" href="#"><i class="material-icons">add</i> 2 More</a> </div>';

    let myActionsCardHead = "<div class='cardHeader'> <h4>My Actions</h4> <a href='#' class='togglePopup' id='actionsCardPopup'><i class='material-icons'> more_vert</i></a> <div class='popup MenuCardPopup' id='actionsCardContent'> <ul class='dashboardMenuPopup'> <li><a href='#'><img src='../dist/images/attendanceBlack.png' alt='attendance'>View My Actions</a> </li> <li><a href='#'><img src='../dist/images/historyBlack.png' alt='history'>View Activities</a></li>  </ul> </div> </div>";

    let myActionsCardContent = '<div class="cardContent"> <ul> <li>Submit investment proof.</li> <li>Interview feedback submission.</li> <li>Health check-up camp in cafetaria at 3pm</li> <li><a href="#">Riya Sharma</a> requested the course Angular Development scheduled on Mar 18th.</li> </ul> </div>';

    // added this function
    function camelize(str) {
        return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
            return index == 0 ? word.toLowerCase() : word.toUpperCase();
        }).replace(/\s+/g, '');
    }
    
    // changed this function
    function cardHTMLTop(text) {
        let headHTML = "";
        let contentHTML = "";
        let currentCard = text.split(" ").join("-");

        switch (currentCard) {
            case "main-attendance-card":
                headHTML = mainAttendanceCardHead;
                contentHTML = mainAttendanceCardContent;
                break;
            case "main-leave-card":
                headHTML = mainLeaveCardHead;
                contentHTML = mainLeaveCardContent;
                break;
            case "main-reimbursement-card":
                headHTML = mainReimbursementCardHead;
                contentHTML = mainReimbursementCardContent;
                break;
            default:
                break;
        }
        return (
            '<div class="col" id=' + camelize(text) + ' data-card=' + text.split(" ").join("-") + '>' + headHTML + contentHTML + '</div>'
        );
    }

    // Changed this function
    function cardHTMLMain(text) {
        let headHTML = "";
        let contentHTML = "";
        let currentCard = text.split(" ").join("-");

        switch (currentCard) {
            case "my-actions":
                headHTML = myActionsCardHead;
                contentHTML = myActionsCardContent;
                break;
            case "my-profile":
                headHTML = profileCardHead;
                contentHTML = profileCardContent;
                break;
            case "quick-links":
                headHTML = quickLinkCardHead;
                contentHTML = quickLinkCardContent;
                break;
            default:
                break;
        }
        return (
            '<div class="col col-md-4 cards" id=' + camelize(text) + ' data-card=' + text.split(" ").join("-") + '>' + headHTML + contentHTML + '</div>'
        );
    }

    // changed lines 253-259
    // Make cards sortable
    $(".connectedSortableMain")
        .sortable({
            connectWith: ".connectedSortableMain",
            placeholder: "ui-state-highlight"
        })
        .disableSelection();
        // changed lines 260- 266
    $(".connectedSortableTop")
    .sortable({
        connectWith: ".connectedSortableTop",
        placeholder: "ui-state-highlight"
    })
    .disableSelection();

    // changed this function
    $("#applyCardsOrder").click(function () {
        sortAndDisplay(".connectedSortableTop li", "topRow");
        sortAndDisplay(".connectedSortableMain li", "mainRow");
    });

    // added this function
    function getCardList(el) {
        return $(el)
        .toArray()
        .filter(function (i) {
            return i.children[1].classList.contains("selectedCard");
        });
    }
    // added this function
    function getCardTypeList(el) {
        return el.map(function (i) {
            return i.attributes[0];
        })
        .map(function (i) {
            return i.nodeValue;
        });
    }

   
    // changed this function
    function sortAndDisplay(list, container) {
        let cardsOrder = [];
        let cardsHtml = "";

        let liArray = getCardList(list);

        let cardTypeList = getCardTypeList(liArray);

        cardsOrder = cardTypeList.map(function (card) {
            return card.split("-").join(" ");
        });

        cardsHtml = cardsOrder.map(function (cardName) {
            switch(container) {
                case "topRow": 
                    return cardHTMLTop(cardName);
                case "mainRow": 
                    return cardHTMLMain(cardName);
                default: break;
            }
        })
        document.getElementById(container).innerHTML = '';
        document.getElementById(container).innerHTML = cardsHtml.join("");
    }

    $('.carousel').carousel({
        interval: false
    })

    // Confetti Code 

    var COLORS, Confetti, NUM_CONFETTI, PI_2, canvas, confetti, context, drawCircle, drawCircle2, drawCircle3, i, range, xpos;
    NUM_CONFETTI = 100;
    COLORS = [
        [235, 90, 70],
        [97, 189, 79],
        [242, 214, 0],
        [0, 121, 191],
        [195, 119, 224]
    ];
    PI_2 = 2 * Math.PI;
    canvas = document.getElementById("confeti");
    context = canvas.getContext("2d");
    window.w = 0;
    window.h = 0;
    window.resizeWindow = function () {
        window.w = canvas.width = window.innerWidth;
        return window.h = canvas.height = window.innerHeight
    };
    window.addEventListener("resize", resizeWindow, !1);
    window.onload = function () {
        return setTimeout(resizeWindow, 0)
    };
    range = function (a, b) {
        return (b - a) * Math.random() + a
    };
    drawCircle = function (a, b, c, d) {
        context.beginPath();
        context.moveTo(a, b);
        context.bezierCurveTo(a - 17, b + 14, a + 13, b + 5, a - 5, b + 22);
        context.lineWidth = 2;
        context.strokeStyle = d;
        return context.stroke()
    };
    drawCircle2 = function (a, b, c, d) {
        context.beginPath();
        context.moveTo(a, b);
        context.lineTo(a + 6, b + 9);
        context.lineTo(a + 12, b);
        context.lineTo(a + 6, b - 9);
        context.closePath();
        context.fillStyle = d;
        return context.fill()
    };
    drawCircle3 = function (a, b, c, d) {
        context.beginPath();
        context.moveTo(a, b);
        context.lineTo(a + 5, b + 5);
        context.lineTo(a + 10, b);
        context.lineTo(a + 5, b - 5);
        context.closePath();
        context.fillStyle = d;
        return context.fill()
    };
    xpos = 0.9;
    document.onmousemove = function (a) {
        return xpos = a.pageX / w
    };
    window.requestAnimationFrame = function () {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (a) {
            return window.setTimeout(a, 5)
        }
    }();
    Confetti = function () {
        function a() {
            this.style = COLORS[~~range(0, 5)];
            this.rgb = "rgba(" + this.style[0] + "," + this.style[1] + "," + this.style[2];
            this.r = ~~range(2, 6);
            this.r2 = 2 * this.r;
            this.replace()
        }
        a.prototype.replace = function () {
            this.opacity = 0;
            this.dop = 0.03 * range(1, 4);
            this.x = range(-this.r2, w - this.r2);
            this.y = range(-20, h - this.r2);
            this.xmax = w - this.r;
            this.ymax = h - this.r;
            this.vx = range(0, 2) + 8 * xpos - 5;
            return this.vy = 0.7 * this.r + range(-1, 1)
        };
        a.prototype.draw = function () {
            var a;
            this.x += this.vx;
            this.y += this.vy;
            this.opacity +=
                this.dop;
            1 < this.opacity && (this.opacity = 1, this.dop *= -1);
            (0 > this.opacity || this.y > this.ymax) && this.replace();
            if (!(0 < (a = this.x) && a < this.xmax)) this.x = (this.x + this.xmax) % this.xmax;
            drawCircle(~~this.x, ~~this.y, this.r, this.rgb + "," + this.opacity + ")");
            drawCircle3(0.5 * ~~this.x, ~~this.y, this.r, this.rgb + "," + this.opacity + ")");
            return drawCircle2(1.5 * ~~this.x, 1.5 * ~~this.y, this.r, this.rgb + "," + this.opacity + ")")
        };
        return a
    }();
    confetti = function () {
        var a, b, c;
        c = [];
        i = a = 1;
        for (b = NUM_CONFETTI; 1 <= b ? a <= b : a >= b; i = 1 <= b ? ++a : --a) c.push(new Confetti);
        return c
    }();
    window.step = function () {
        var a, b, c, d;
        requestAnimationFrame(step);
        context.clearRect(0, 0, w, h);
        d = [];
        b = 0;
        for (c = confetti.length; b < c; b++) a = confetti[b], d.push(a.draw());
        return d
    };
    step();


});