(function ($) {
    $.confirm = function (params) {
        if ($('#messageOverlay').length) {
            // A confirm is already shown on the page:
            return false;
        }

        var buttonHTML = '';
        $.each(params.buttons, function (name, obj) {
            buttonHTML += '<a href="#" class="button ' + obj['class'] + '">' + name + '<span></span></a>';
            if (!obj.action) {
                obj.action = function () { };
            }
        });

        var markup = [
			'<div id="messageOverlay">',
			'<div id="confirmBox">',
			'<h1>', params.title, '</h1>',
			'<p>', params.message, '</p>',
			'<div id="messageButtons">',
			buttonHTML,
			'</div></div></div>'
		].join('');

        $(markup).hide().appendTo('body').fadeIn();

        var buttons = $('#confirmBox .button'),
			i = 0;

        $.each(params.buttons, function (name, obj) {
            buttons.eq(i++).click(function () {
                // Calling the action attribute when a
                // click occurs, and hiding the confirm.
                obj.action();
                $.confirm.hide();
                return false;
            });
        });
    }
    $.confirm.hide = function () {
        $('#messageOverlay').fadeOut(function () {
            $(this).remove();
        });
    }

    $.alert = function (params) {
        if ($('#messageOverlay').length) {
            // A alert is already shown on the page:
            return false;
        }

        var buttonHTML = '';
        $.each(params.buttons, function (name, obj) {
            buttonHTML += '<a href="#" class="button ' + obj['class'] + '">' + name + '<span></span></a>';
            if (!obj.action) {
                obj.action = function () { };
            }
        });

        var markup = [
			'<div id="messageOverlay">',
			'<div id="alertBox">',
			'<h1>', params.title, '</h1>',
			'<p>', params.message, '</p>',
			'<div id="messageButtons">',
			buttonHTML,
			'</div></div></div>'
		].join('');

        $(markup).hide().appendTo('body').fadeIn();

        var buttons = $('#alertBox .button'),
			i = 0;

        $.each(params.buttons, function (name, obj) {
            buttons.eq(i++).click(function () {
                // Calling the action attribute when a
                // click occurs, and hiding the alert.
                obj.action();
                $.alert.hide();
                return false;
            });
        });
    }
    $.alert.hide = function () {
        $('#messageOverlay').fadeOut(function () {
            $(this).remove();
        });
    }

    $.inform = function (params) {
        if ($('#messageOverlay').length) {
            // A alert is already shown on the page:
            return false;
        }

        var buttonHTML = '';
        $.each(params.buttons, function (name, obj) {
            buttonHTML += '<a href="#" class="button ' + obj['class'] + '">' + name + '<span></span></a>';
            if (!obj.action) {
                obj.action = function () { };
            }
        });

        var markup = [
            '<div id="messageOverlay">',
            '<div id="informBox">',
            '<h1>', params.title, '</h1>',
            '<p>', params.message, '</p>',
            '<div id="messageButtons">',
            buttonHTML,
            '</div></div></div>'
        ].join('');

        $(markup).hide().appendTo('body').fadeIn();

        var buttons = $('#informBox .button'),
            i = 0;

        $.each(params.buttons, function (name, obj) {
            buttons.eq(i++).click(function () {
                // Calling the action attribute when a
                // click occurs, and hiding the alert.
                obj.action();
                $.inform.hide();
                return false;
            });
        });
    }
    $.inform.hide = function () {
        $('#messageOverlay').fadeOut(function () {
            $(this).remove();
        });
    }

})(jQuery);