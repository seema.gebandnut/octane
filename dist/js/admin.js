// Initialize popup
$("#configurationPopup, #resignationPopup, #holdUnholdPopup, #selectAgencyPopup, #weeklyOffSettingPopup, #listAgendaPopup, #rejectPopup, #approvePopup, #employeeSelectPopup,#reEvaluatePopup, #workingPopup, #assetLocationPopup, #attachmentPopup, #leaveAdjustmentDetailPopup,#cancelPopup,#historyDetailsPopup,#selectEmployeePopup,#onboardingFormPopup,#addFilterPopup,#additionalAttendancePopup,#configurationPopup,#weeklyOffSettingPopups,#additionalBranchTimingsPopup,#additionalBranchTimingsPopups,#uploadHolidaysPopup,#assetdetailsPopup,#salaryComparisonPopup,#talentFeedbackPopup,#assetIssuePopup,#finalDecisionPopup,#answerPopuo,#documentSubmissionStatusPopup,#bgvSentDatePopup,#bgvRecievedDatePopup,#selectTemplatesPopup,#holdUnholdPopup,#increaseVacancyPopup,#assignRecruiterPopuop,#acknowledgeRequestPopup,#returnRequestPopup,#attendanceDetailsPopup,#attachmentpopupcard, #glAccountPopup, #glAccountSettingPopup, #cancelRequestPopup, #itCategoryPopup, #limitForAccountPopup").dialog({
  autoOpen: false,
  modal: true,
  closeOnEscape: true
});

function showpopup(openThis) {
  popUp(openThis, "open");
  $(openThis).css({
    visibility: "visible"
  })
}

// Open popup on click
$(document).on("click", "#acknowledgeRequest", function () {
  showpopup("#acknowledgeRequestPopup")
});
$(document).on("click", "#resignation", function () {
  showpopup("#resignationPopup")
});

$(document).on("click", "#attachmentCard", function () {
  showpopup("#attachmentpopupcard")
});

$(document).on("click", "#returnRequest", function () {
  showpopup("#returnRequestPopup")
});

$(document).on("click", "#attendanceDetails", function () {
  showpopup("#attendanceDetailsPopup")
});

$(document).on("click", "#bgvRecievedDate", function () {
  showpopup("#bgvRecievedDatePopup")
});

$(document).on("click", "#cancelRequest", function () {
  showpopup("#cancelRequestPopup")
});

$(document).on("click", "#selectTemplates", function () {
  showpopup("#selectTemplatesPopup")
});

$(document).on("click", "#bgvSentDate", function () {
  showpopup("#bgvSentDatePopup")
});

$(document).on("click", "#answer", function () {
  showpopup("#answerPopuo")
});

$(document).on("click", "#documentSubmissionStatus", function () {
  showpopup("#documentSubmissionStatusPopup")
});

$(document).on("click", "#selectAgency", function () {
  showpopup("#selectAgencyPopup")
});

$(document).on("click", "#assetIssue", function () {
  showpopup("#assetIssuePopup")
});

$(document).on("click", "#itCategory", function () {
  showpopup("#itCategoryPopup")
});

$(document).on("click", "#limitForAccount", function () {
  showpopup("#limitForAccountPopup")
});

$(document).on("click", "#glAccountSetting", function () {
  showpopup("#glAccountSettingPopup")
});

$(document).on("click", "#holdUnhold", function () {
  showpopup("#holdUnholdPopup")
});

$(document).on("click", "#glAccount", function () {
  showpopup("#glAccountPopup")
});

$(document).on("click", "#configuration", function () {
  showpopup("#configurationPopup")
});

$(document).on("click", "#weeklyOffSetting", function () {
  showpopup("#weeklyOffSettingPopup")
});

$(document).on("click", "#listAgenda", function () {
  showpopup("#listAgendaPopup")
});

$(document).on("click", "#approve", function () {
  showpopup("#approvePopup")
});

$(document).on("click", "#reject", function () {
  showpopup("#rejectPopup")
});

$(document).on("click", "#employeeSelect,#empSearch", function () {
  showpopup("#employeeSelectPopup")
});

$(document).on("click", "#leaveAdjustmentDetail", function () {
  showpopup("#leaveAdjustmentDetailPopup")
});

$(document).on("click", "#cancelButton", function () {
  showpopup("#cancelPopup")
});

$(document).on("click", "#historyDetails", function () {
  showpopup("#historyDetailsPopup")
});

$(document).on("click", "#selectEmployee", function () {
  showpopup("#selectEmployeePopup")
});

$(document).on("click", "#addFilter", function () {
  showpopup("#addFilterPopup")
});

$(document).on("click", "#onboardingForm", function () {
  showpopup("#onboardingFormPopup")
});

$(document).on("click", "#additionalAttendance", function () {
  showpopup("#additionalAttendancePopup")
});

$(document).on("click", "#configurationOpen", function () {
  showpopup("#configurationPopup")
});

$(document).on("click", "#weeklyOffSettings", function () {
  showpopup("#weeklyOffSettingPopups")
});

$(document).on("click", "#additionalBranchTimings", function () {
  showpopup("#additionalBranchTimingsPopup")
});

$(document).on("click", "#additionalBranchTimingss", function () {
  showpopup("#additionalBranchTimingsPopups")
});

$(document).on("click", "#uploadHolidays", function () {
  showpopup("#uploadHolidaysPopup")
});

$(document).on("click", "#salaryComparison", function () {
  showpopup("#salaryComparisonPopup")
});

$(document).on("click", "#talentFeedback", function () {
  showpopup("#talentFeedbackPopup")
});
$(document).on("click", "#assetdetails", function () {
  showpopup("#assetdetailsPopup")
});

$(document).on("click", "#finalDecision", function () {
  showpopup("#finalDecisionPopup")
});


$(document).on("change", "#recruitmentDropdownPopup", function () {
  if (this.value == 'holdUnhold') {
    popUp("#holdUnholdPopup", "open");
    $("#holdUnholdPopup").css({
      visibility: "visible"
    });
  } else if (this.value == 'close') {
    popUp("#closePopup", "open");
    $("#closePopup").css({
      visibility: "visible"
    });
  } else if (this.value == 'increaseVacancies') {
    popUp("#increaseVacancyPopup", "open");
    $("#increaseVacancyPopup").css({
      visibility: "visible"
    });
  } else if (this.value == 'assignRecruiter') {
    popUp("#assignRecruiterPopuop", "open");
    $("#assignRecruiterPopuop").css({
      visibility: "visible"
    });
  }
});

$(document).on("change", "#dropdownFilter", function () {


  if (this.value == 'Attachments') {
    popUp("#attachmentPopup", "open");
    $("#attachmentPopup").css({
      visibility: "visible"
    });
  } else if (this.value == 'Revaluate') {
    popUp("#reEvaluatePopup", "open");
    $("#reEvaluatePopup").css({
      visibility: "visible"
    });
  } else if (this.value == 'Working') {
    popUp("#workingPopup", "open");
    $("#workingPopup").css({
      visibility: "visible"
    });
  } else if (this.value == 'Location') {
    popUp("#assetLocationPopup", "open");
    $("#assetLocationPopup").css({
      visibility: "visible"
    });
  }
});


// Close popup on click
$("#closePopup, #closePopupBtn, #applyCardsOrder").on("click", function () {
  popUp("#weeklyOffSettingPopup", "close");
  popUp("#assignRecruiterPopuop", "close");
  popUp("#holdUnholdPopup", "close");
  popUp("#increaseVacancyPopup", "close");
  popUp("#additionalAttendancePopup", "close");
  popUp("#resignationPopup", "close");
  popUp("#configurationPopup", "close");
  popUp("#additionalBranchTimingsPopup", "close");
  popUp("#assetdetailsPopup", "close");
  popUp("#weeklyOffSettingPopups", "close");
  popUp("#additionalBranchTimingsPopups", "close");
  popUp("#attachmentpopupcard", "close");
  popUp("#returnRequestPopup", "close");
  popUp("#leaveAdjustmentDetailPopup", "close");
  popUp("#documentSubmissionStatusPopup", "close");
  popUp("#selectTemplatesPopup", "close");
  popUp("#selectAgencyPopup", "close");
  popUp("#answerPopuo", "close");
  popUp("#attendanceDetailsPopup", "close");
  popUp("#acknowledgeRequestPopup", "close");
  popUp("#bgvRecievedDatePopup", "close");
  popUp("#glAccountPopup", "close");
  popUp("#listAgendaPopup", "close");
  popUp("#uploadHolidaysPopup", "close");
  popUp("#limitForAccountPopup", "close");
  popUp("#cancelRequestPopup", "close");
  popUp("#assetIssuePopup", "close");
  popUp("#finalDecisionPopup", "close");
  popUp("#itCategoryPopup", "close");
  popUp("#configurationPopup", "close");
  popUp("#glAccountSettingPopup", "close");
  popUp("#approvePopup", "close");
  popUp("#rejectPopup", "close");
  popUp("#employeeSelectPopup", "close");
  popUp("#bgvSentDatePopup", "close");
  popUp("#salaryComparisonPopup", "close");
  popUp("#holdUnholdPopup", "close");
  popUp("#cancelPopup", "close");
  popUp("#historyDetailsPopup", "close");
  popUp("#addFilterPopup", "close");
  popUp("#selectEmployeePopup", "close");
  popUp("#talentFeedbackPopup", "close");
  popUp("#attachmentPopup", "close");
  popUp("#reEvaluatePopup", "close");
  popUp("#assetLocationPopup", "close");
  popUp("#workingPopup", "close");
});

// Function for closing popup
function popUp(element, action) {
  $(element).dialog(action);
  return false;
}