﻿var map, watchId, marker, circle;
var options = {
    enableHighAccuracy: true,
    timeout: 20000,
    maximumAge: 0
};
var options_nonMobile = {
    enableHighAccuracy: false,
    timeout: 5000,
    maximumAge: 0
};
$(window).unload(function () {
    if (watchId != undefined) {
        navigator.geolocation.clearWatch(watchId);
    }
});
function showCurrentLocation(nonMobile) {
    $("#map").html("<div style='font-weight: bold; font-size: 12px; text-align: center;vertical-align: middle; top: 50%;left: 50%;transform: translate(-50%,-50%);position: absolute;'>Getting your location <br/> Please wait ....</div>");
    if (nonMobile == true) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                showCurrentPosition(position, nonMobile);
            },
                function () { $("#map").html("<div style='font-weight: bold; font-size: 12px; color:#FD0000;text-align: center;vertical-align: middle; top: 50%;left: 50%;transform: translate(-50%,-50%);position: absolute;'>Location Data Unavailable.</div>"); },
                options_nonMobile);
        }
    }
    else {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                showCurrentPosition(position, nonMobile);
            },
                function (error) { showLocationError(error, nonMobile); },
                options);
        } else {
            generate('error', 'Location is not supported by this browser.');
        }
    }

}
function showCurrentPosition(position, nonMobile) {
    var DisplayAddressOnMap = '';
    try {
        $.ajax({
            type: 'POST',
            url: '/_WebServices/1_InternalServices.asmx/DisplayAddressOnMap',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                DisplayAddressOnMap = data.d;
            },
            error: function (xhr) { }
        });
    }
    catch (ex) { }

    if (nonMobile == true) {
       
        if (position.coords.accuracy > 1000) {
            $("#map").html("<div style='font-weight: bold; font-size: 12px; color:#FD0000;text-align: center;vertical-align: middle; top: 50%;left: 50%;transform: translate(-50%,-50%);position: absolute;'>Location Accuracy Exceeds Permissible Range.</div>");
        }
        else {
            loadMap(position.coords.latitude, position.coords.longitude, position.coords.accuracy);
            displayMarker(position.coords.latitude, position.coords.longitude, position.coords.accuracy);
            if (DisplayAddressOnMap == "Y") {
                displayAddress(position.coords.latitude, position.coords.longitude);
            }
        }
    }
    else {
        loadMap(position.coords.latitude, position.coords.longitude, position.coords.accuracy);
        displayMarker(position.coords.latitude, position.coords.longitude, position.coords.accuracy);
        if (DisplayAddressOnMap == "Y") {
            watchId = navigator.geolocation.watchPosition(function (position) { displayMarker(position.coords.latitude, position.coords.longitude, position.coords.accuracy); displayAddress(position.coords.latitude, position.coords.longitude); },
                function () { removeMarker(); removeAddress(); },
                options)
        }
        else {
            watchId = navigator.geolocation.watchPosition(function (position) { displayMarker(position.coords.latitude, position.coords.longitude, position.coords.accuracy); },
                function () { removeMarker(); removeAddress(); },
                options)
        }
    }
}
function showSavedPosition(latitude, longitude, accuracy, addr) {
    setTimeout(function () {
        loadMap(latitude, longitude, accuracy);
        displayMarker(latitude, longitude, accuracy);
        if (addr != '') {
            $("#map").append("<div id='curraddr' class='address'>" + addr + "</div>");
        }
    }, 500);
}

function showLocationError(error, nonMobile) {
    $("#map").html("<div style='font-weight: bold; font-size: 12px; color:#FD0000;text-align: center;vertical-align: middle; top: 50%;left: 50%;transform: translate(-50%,-50%);position: absolute;'>Location Data Unavailable.</div>");
    switch (error.code) {
        case error.PERMISSION_DENIED:
            if (nonMobile == true) {
                $.alert({
                    'title': '',
                    'message': 'Allow Location Service.',
                    'buttons': {
                        'Ok': {
                            'class': 'ok',
                            'action': function () {
                            }
                        }
                    }
                });
            }
            else {
                generate('error', 'Allow Location Service.');
            }
            break;
        case error.POSITION_UNAVAILABLE:
            if (nonMobile == true) {
                $.alert({
                    'title': '',
                    'message': 'Location information is unavailable.',
                    'buttons': {
                        'Ok': {
                            'class': 'ok',
                            'action': function () {
                            }
                        }
                    }
                });
            }
            else {
                generate('error', 'Location information is unavailable.');
            }
            break;
        case error.TIMEOUT:
            if (nonMobile == true) {
                $.alert({
                    'title': '',
                    'message': 'Operation Timed Out.',
                    'buttons': {
                        'Ok': {
                            'class': 'ok',
                            'action': function () {
                            }
                        }
                    }
                });
            }
            else {
                generate('error', 'Operation Timed Out.');
            }
            break;
        case error.UNKNOWN_ERROR:
            if (nonMobile == true) {
                $.alert({
                    'title': '',
                    'message': 'An unknown error occurred.',
                    'buttons': {
                        'Ok': {
                            'class': 'ok',
                            'action': function () {
                            }
                        }
                    }
                });
            }
            else {
                generate('error', 'An unknown error occurred.');
            }
            break;
    }
}

function loadMap(latitude, longitude, accuracy) {
    var coords = new google.maps.LatLng(latitude, longitude);
    var mapproperty = {
        center: coords,
        mapTypeControl: false,
        panControl: false,
        streetViewControl: false,
        navigationControlOptions: {
            style: google.maps.NavigationControlStyle.SMALL
        },
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map"), mapproperty);
}

function displayMarker(latitude, longitude, accuracy) {
    var coords = new google.maps.LatLng(latitude, longitude);
    var screenSize = Math.min($("#map").height(), $("#map").width());
    var equator = 40075160;
    var requiredMpp = accuracy / screenSize;
    var zoomLevel = Math.min(Math.floor((Math.log(equator / (256 * requiredMpp))) / Math.log(2)) - 1, 16);
    map.setZoom(zoomLevel);
    var icon = {
        path: google.maps.SymbolPath.CIRCLE,
        scale: 8,
        fillColor: "#ad1315",
        fillOpacity: 1,
        strokeWeight: 5,
        strokeColor: "#00406e"
    }
    if (marker != undefined) {
        marker.setPosition(coords);
        marker.setIcon(icon);
    }
    else {
        marker = new google.maps.Marker({
            position: coords,
            map: map,
            draggable: false,
            icon: icon
        });
    }
    map.setCenter(marker.getPosition());
    if (circle != undefined) {
        circle.setMap(null);
    }
    circle = new google.maps.Circle({
        map: map,
        radius: parseInt(accuracy),
        fillColor: '#c21b00',
        fillOpacity: 0.3,
        strokeWeight: 0
    });
    circle.bindTo('center', marker, 'position');
}

function removeMarker() {
    var icon = {
        path: google.maps.SymbolPath.CIRCLE,
        scale: 8,
        fillColor: "#ffffff",
        fillOpacity: 0.1,
        strokeWeight: 5,
        strokeColor: "#aaaaaa"
    }
    if (marker != undefined) {
        marker.setIcon(icon);
    }

    if (circle != undefined) {
        circle.setMap(null);
    }
}

function getAddress(position) {
    var APIKey = '';
    $.ajax({
        type: 'POST',
        url: '/_WebServices/1_InternalServices.asmx/getGoogleMapAPIKey',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            APIKey = data.d;
        },
        error: function (xhr) {
            throw (JSON.parse(xhr.responseText).Message);
        }
    });
    var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + position.coords.latitude + "," + position.coords.longitude + "&sensor=true&key=" + APIKey;
    $.ajax({
        type: 'GET',
        url: url,
        async: false,
        success: function (data) {
            document.getElementById("CurrAddress").value = data.results[0].formatted_address;
        },
        error: function (xhr) {
            throw (JSON.parse(xhr.responseText).Message);
        }
    });
}

function displayAddress(latitude, longitude) {
    var APIKey = '';
    $.ajax({
        type: 'POST',
        url: '/_WebServices/1_InternalServices.asmx/getGoogleMapAPIKey',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            APIKey = data.d;
        },
        error: function (xhr) { }
    });
    var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&sensor=true&key=" + APIKey;
    $.getJSON(url, function (data, textStatus) {
        var streetaddress = data.results[0].formatted_address;
        try {
            $("#curraddr").remove();
        }
        catch (ex) { }
        $("#map").append("<div id='curraddr' class='address'>" + streetaddress + "</div>");
        document.getElementById("CurrAddress").value = streetaddress;
    });
}

function removeAddress() {
    try {
        $("#curraddr").remove();
    }
    catch (ex) { }
}

function getCurrentLocation(nonMobile, callback) {
    if (nonMobile == true) {
        try {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    if (position.coords.accuracy <= 1000) {
                        document.getElementById("CurrLatitude").value = position.coords.latitude;
                        document.getElementById("CurrLongitute").value = position.coords.longitude;
                        document.getElementById("CurrAccuracy").value = position.coords.accuracy;
                        //getAddress(position);
                        
                    }
                    
                    callback();
                },
                    function () {
                        //stopUpdateProgress();
                        callback();
                        
                    },
                    options_nonMobile);
                
            }
            else {
                callback();
            }
        }
        catch (err) {
            $.alert({
                'title': '',
                'message': err.message,
                'buttons': {
                    'Ok': {
                        'class': 'ok',
                        'action': function () {
                        }
                    }
                }
            });
        }
    }
    else {
        try {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    document.getElementById("CurrLatitude").value = position.coords.latitude;
                    document.getElementById("CurrLongitute").value = position.coords.longitude;
                    document.getElementById("CurrAccuracy").value = position.coords.accuracy;
                    getAddress(position);
                    SetCurrentLocation(callback);
                },
                    function (error) {
                        stopUpdateProgress();
                        showLocationError(error, nonMobile);
                    },
                    options);
            } else {
                generate('error', 'Location is not supported by this browser.');
            }
        }
        catch (err) {
            generate('error', err.message);
        }
    }
}