/*****Explore learning detail page functions **********/

//Clear filter criteria
$('.filterCross,.ClearCross').click(function (e) {

	$('#filterFrm input:checkbox').prop('checked', false);
	$('#filterFrm input:radio').prop('checked', false);

	$("#filterFrm").find(".active").removeClass('active');
	$('input,textarea').val('');
	$('select').prop("selectedIndex", 0);

	var search = '<li><a href="#filter" rel="modal:open"><label class="btn btn-default"><div>Add Filters</div></label></a></li>';
	$('#filterBar').html(search);
	$('.filterCross').empty();

}); //End function

//Cancel Filter popup form
$('#cancelFilter').click(function (e) {

	$('#filterFrm')[0].reset();

	$('.jquery-modal').hide();
	$(".modal").hide();


}); //End function


//Remove filter elements
function removeVal(val, elementType) {
	$.each($("input[name='levels']:checked"), function () {

		if ($(this).val() == val) {
			$(this).prop('checked', false);
			$(this).parent().removeClass('active');
		}
	});
	$.each($("input[name='type']:checked"), function () {
		if ($(this).val() == val) {
			$(this).prop('checked', false);
			$(this).parent().removeClass('active');
		}
	});
	$.each($("input[name='school']:checked"), function () {
		if ($(this).val() == val) {
			$(this).prop('checked', false);
			$(this).parent().removeClass('active');
		}
	});
	$.each($("input[name='duration']:checked"), function () {
		if ($(this).val() == val) {
			$(this).prop('checked', false);
			$(this).parent().removeClass('active');
		}
	});

	$.each($("input[name='costType']:checked"), function () {
		if ($(this).val() == val) {

			$(this).prop("checked", false);
			$(this).parent().removeClass('active');
		}
	});


	applyFilters();

	return false;
}


// Explore all filter search function
function applyFilters() {

	var filterSearch = [];

	$.each($("input[name='levels']:checked"), function () {

		filterSearch.push(`<li id=${$(this).val()}>
    <label class="btn btn-default">
      <div>${$(this).val()}
        <i onclick=removeVal("${$(this).val()}","levels") class="material-icons">close</i>
      </div>
    </label>
  </li>`);
	});
	$.each($("input[name='type']:checked"), function () {
		filterSearch.push(`<li id=${$(this).val()}>
  <label class="btn btn-default">
    <div>${$(this).val()}
      <i onclick=removeVal("${$(this).val()}","type") class="material-icons">close</i>
    </div>
  </label>
</li>`);
	});

	$.each($("input[name='school']:checked"), function () {
		filterSearch.push(`<li id=${$(this).val()}>
  <label class="btn btn-default">
    <div>${$(this).val()}
      <i onclick=removeVal("${$(this).val()}","school") class="material-icons">close</i>
    </div>
  </label>
</li>`);
	});
	$.each($("input[name='duration']:checked"), function () {
		filterSearch.push(`<li id=${$(this).val()}> <label class="btn btn-default"><div>${$(this).val()}<i onclick=removeVal("${$(this).val()}","duration") class="material-icons">close</i></div></label></li>`);
	});

	var costType = $("input[name='costType']:checked").val();

	if (costType != '' && costType != undefined) {
		filterSearch.push(`<li id=${costType}><label class="btn btn-default"><div>${costType}<i onclick=removeVal("${costType}","school") class="material-icons">close</i></div></label></li>`);
	}

	var allFilterlen = filterSearch.length;
	var search = filterSearch;

	if (allFilterlen == 0) {
		search = `<li><a href="#filter" rel="modal:open"><label class="btn btn-default"><div>Add Filters</div></label></a></li>`;
		$('.filterCross').empty();
}
if (allFilterlen >= 1) {
		var search = filterSearch.join('');
		search = `${search} <li><a href="#filter" rel="modal:open"><label class="btn btn-default"><div>Add Filters</div></label></a></li>`;
		$('.filterCross').html('Clear');
}
if (allFilterlen > 3) {
		var restLen = allFilterlen - 3
		var search = filterSearch.slice(0, 3).join('');
		search = `${search} <li><label class="btn btn-default"><div>${restLen} more</div></label></li><li><a href="#filter" rel="modal:open"><label class="btn btn-default"><div>Add Filters</div></label></a></li>`;
		$('.filterCross').html('Clear');
}


	$('#filterBar').html(search);
	$('body').css('overflow', 'auto');
	$('.jquery-modal').hide();
	$(".modal").hide();


} //End function


