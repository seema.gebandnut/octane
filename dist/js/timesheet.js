// Initialize popup
$("#cancelPopup").dialog({
	autoOpen: false,
	modal: true,
	closeOnEscape: true
  });
  // Open popup on click
  $(document).on("click", "#cancelButton", function () {
	popUp("#cancelPopup", "open");
	$("#cancelPopup").css({
		visibility: "visible"
	});
  });
  // Close popup on click
$("#closePopup, #closePopupBtn, #applyCardsOrder").on("click", function () {
	popUp("#cancelPopup", "close");
});
// Function for closing popup
function popUp(element, action) {
	$(element).dialog(action);
	return false;
  }
 //Clear filter criteria in popup
$('.ClearCross, .filterCross').click(function (e) {
	$('input,textarea').val('');
	$('select').prop("selectedIndex", 0);
    resetFilters([
        "#projectType",
        "#branchType",
        "#employmentType",
        "#companyType",
        "#costBranchType",
        "#departmentType"
    ], "#leavesfilter", "#filterBarLeaves");
}); //End function	 
  