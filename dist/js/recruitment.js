// Radio button show hide div on click
document.getElementById('opt1').addEventListener('click', function opt1() {
	document.getElementById('hide12').style.display = 'none';
});
document.getElementById('opt2').addEventListener('click', function opt2() {
	document.getElementById('hide12').style.display = 'block';
});


// Radio button show hide div on click
document.getElementById('opt3').addEventListener('click', function opt3() {

	document.getElementById('hide121').style.display = 'block';
});
document.getElementById('opt4').addEventListener('click', function opt4() {
	document.getElementById('hide121').style.display = 'none';
});



//Employee search filter
	var availableTags = [
	"Suman",
	"Rana",
	"Manoj",
	"Praveen",
	"Priyank",
	"Ashok",
	"Amar",
	"Rajveer"				
  ];
  $("#empSearch,#reportingTo").autocomplete({
	source: availableTags
  });
  
  
 // Skills Tag Code 
 $('#skills').select2({
	 placeholder: "Enter your skills",
	 tags: true,
	 width: "200px"
 });
 
  // Skills Tag Code 
 $('#skills2').select2({
	 placeholder: "Enter your skills",
	 tags: true,
	 width: "200px"
 });
 
 //Skills validation
 $("#skills").change(function () {
	  var skills = $('#skills').children('option').length;
			
		if(skills<1){
			$('#manSkills .select2-selection--multiple').addClass('error');
			$('#manSkills .dropdown-wrapper').addClass('error');
			$('#manSkills .dropdown-wrapper').html('Please enter your skills');
			$('html, body').animate({
					scrollTop: $('#skills').offset().top-200
				});
			
		}else{
			
			$('#manSkills .select2-selection--multiple').removeClass('error');
			$('#manSkills .dropdown-wrapper').html('');
		}
});

 
//Upload Document Code
var $fileInput = $('.file-input');
var $droparea = $('.file-drop-area');

// highlight drag area
$fileInput.on('dragenter focus click', function() {
  $droparea.addClass('is-active');
});

// back to normal state
$fileInput.on('dragleave blur drop', function() {
  $droparea.removeClass('is-active');
});

// change inner text
$fileInput.on('change', function() {

  var filesCount = $(this)[0].files.length;
  var $textContainer = $(this).prev();

  if (filesCount === 1) {
    // if single file is selected, show file name
    var fileName = $(this).val().split('\\').pop();
    $textContainer.text(fileName);
  } 
  /*else {
    // otherwise show number of files
    $textContainer.text(filesCount + ' files selected');
  }*/
});
//End File Upload Code


//Remove Row from Table
$("a.removeRow").click(function () {
	$(this).parents("tr").remove();
	
});

function removeRow(id){
	$('#'+id).remove();
}


$( "#question" ).change(function () {
	if($( "#question" ).val()!=''){
		$('#question').removeClass('error');
		$('#ques-error').html('');
	}
	
});



//Add More Questions in to Table
var c=1;
$("#Customjobdesc").click(function () {
	var job_desc = $('#job_desc').val();
	if(job_desc==''){
		$('#job_desc').addClass('error');
		$('#job_desc').focus();
		$('#job_desc-error').html('Please enter question');
	}else{
		var idVal = 'job_desc'+c;
		$('#jobdescTable').append('<tr id="'+idVal+'"><td>'+job_desc+'</td><td class="actionButton dropBtn"><a href="javascript:void(0)" onclick=removeRow("'+idVal+'") class="removeRow">Remove</a></td></tr>');
		$('#job_desc').removeClass('error');
		$('#job_desc-error').html('');
		$('#job_desc').val('');
		c=c+1;
	}
});



//Add More Questions in to Table
var i=1;
$("#customQues").click(function () {
	var ques = $('#question').val();
	if(ques==''){
		$('#question').addClass('error');
		$('#question').focus();
		$('#ques-error').html('Please enter question');
	}else{
		var idVal = 'ques'+i;
		$('#quesTable').append('<tr id="'+idVal+'"><td>'+ques+'</td><td class="actionButton dropBtn"><a href="javascript:void(0)" onclick=removeRow("'+idVal+'") class="removeRow">Remove</a></td></tr>');
		$('#question').removeClass('error');
		$('#ques-error').html('');
		$('#question').val('');
		i=i+1;
	}
});


//Add More qualification in to Table
var j=1;
$("#qualificationAdd").click(function () {
	var qualification = $('#qualification').val();
	if($('#mandatory').prop('checked') == true){
		var mandatory = 'Yes';
	
	}else{
		var mandatory = 'No';
	}
	
	if(qualification==''){
		$('#qualification').addClass('error');
		$('#qualification').focus();
		$('#qualification-error').show();
	}else{
		var idVal = 'qual'+j;
		$('#qualificationTbl').append('<tr id="'+idVal+'"><td>'+qualification+'</td><td>'+mandatory+'</td><td class="actionButton dropBtn"><a href="javascript:void(0)"  onclick=removeRow("'+idVal+'") class="removeRow">Remove</a></td></tr>');
		$('#qualification').removeClass('error');
		$('#qualification-error').hide();
		
		j=j+1;
	}
});


//Add More mandatory Skills in to Table
var m=1;
$("#customSkills").click(function () {
	var mandatorySkills = $('#mandatorySkills').val();
	
	if(mandatorySkills==''){
		$('#mandatorySkills').addClass('error');
		$('#mandatorySkills').focus();
		$('#mandatorySkills-error').show();
	}else{
		var idVal = 'skills'+m;
		$('#skillsTable').append('<tr id="'+idVal+'"><td>'+mandatorySkills+'</td><td class="actionButton dropBtn"><a href="javascript:void(0)"  onclick=removeRow("'+idVal+'") class="removeRow">Remove</a></td></tr>');
		$('#mandatorySkills').removeClass('error');
		$('#mandatorySkills-error').hide();
		$('#mandatorySkills').val('');
		
		m=m+1;
	}
});



//Add More Good to have Skills in to Table
var n=1;
$("#customgoodHaveSkills").click(function () {
	var goodHaveSkills = $('#goodHaveSkills').val();
	
	if(goodHaveSkills==''){
		$('#goodHaveSkills').addClass('error');
		$('#goodHaveSkills').focus();
		$('#goodHaveSkills-error').show();
	}else{
		var idVal = 'HaveSkills'+n;
		$('#goodHaveSkillsTable').append('<tr id="'+idVal+'"><td>'+goodHaveSkills+'</td><td class="actionButton dropBtn"><a href="javascript:void(0)"  onclick=removeRow("'+idVal+'") class="removeRow">Remove</a></td></tr>');
		$('#goodHaveSkills').removeClass('error');
		$('#goodHaveSkills-error').hide();
		$('#goodHaveSkills').val('');
		n=n+1;
	}
});



$( "#company" ).change(function () {
	if($( "#company" ).val()!=''){
		$('#company').removeClass('error');
		$('#company-error').hide();
	}
	
});
//Add More Company in to Table
var k=1;
$("#companyAdd").click(function () {
	var company = $('#company').val();
	if(company==''){
		$('#company').addClass('error');
		$('#company').focus();
		$('#company-error').show();
	}else{
		var idVal = 'com'+k;
		$('#companyTable').append('<tr id="'+idVal+'"><td>'+company+'</td><td class="actionButton dropBtn"><a href="javascript:void(0)" onclick=removeRow("'+idVal+'") class="removeRow">Remove</a></td></tr>');
		$('#company').removeClass('error');
		$('#company-error').hide();
		$('#company').val('');
		k=k+1;
	}
});

$( "#employee" ).change(function () {
	if($( "#employee" ).val()!=''){
		$('#employee').removeClass('error');
		$('#employee-error').hide();
	}
	
});

//Add More Company in to Table
var l=1;
$("#empAdd").click(function () {
	var employee = $('#employee').val();
	if(employee==''){
		$('#employee').addClass('error');
		$('#employee').focus();
		$('#employee-error').show();
	}else{
		var idVal = 'emp'+l;
		$('#empTable').append('<tr id="'+idVal+'"><td>'+employee+'</td><td>Name</td><td class="actionButton dropBtn"><a href="javascript:void(0)" onclick=removeRow("'+idVal+'") class="removeRow">Remove</a></td></tr>');
		$('#employee').removeClass('error');
		$('#employee-error').hide();
		$('#employee').val('');
		l=l+1;
	}
});

/* Updated on 5/29/2019 by Seema */
$("#dob").datepicker({
	   dateFormat: 'dd/mm/yy',
	    changeYear: true,
		yearRange: '1970:2019',
		 onSelect: function () {
			if ($("#dob").val() != '') {
				$("#dob").removeClass('error');
				$('#dob-error').empty();
			}
                  
         }
				
});


//show datepicker on cal icon
$(".cal1").click(function () {

	$('#dob').datepicker("show");

});


//Progress Bar Code Start 
/* Updated on 4/18/2019 By Seema */

//Intiate Next click action
$(".step1Next").click(function () {
	
	var check = formValidations('initiateSection');

	if(check){
		$('#initiateSection').hide();
		$('#profileSection').show();
		$('#step2').addClass('formFilled');
		$('#step1').removeClass('formFilled');
		$('.progress-bar').css('width','20%');
		 $('html, body').animate({
			scrollTop: $("#formProgress").offset().top-200
		}, 1000);
	}else{
		return false;
	}
	
});

//Profile Section Next click action
$(".step2Next").click(function () {
	var check = formValidations('profileSection');

	if(check){
		$('#profileSection').hide();
		$('#jobSection').show();
		$('#step3').addClass('formFilled');
		$('#step2').removeClass('formFilled');
		$('.progress-bar').css('width','39%');
		 $('html, body').animate({
			scrollTop: $("#formProgress").offset().top-200
		}, 1000);
	}else{
		return false;
	}
});
$(".step2Back").click(function () {
	$('#profileSection').hide();
	$('#initiateSection').show();
	$('.progress-bar').css('width','3%');
	 $('html, body').animate({
			scrollTop: $("#formProgress").offset().top-200
		}, 1000);
});

//Job Section Next click action
$(".step3Next").click(function () {

	var skills = $('#skills').children('option').length;
	
	if(skills<1){
		$('#manSkills .select2-selection--multiple').addClass('error');
		$('#manSkills .dropdown-wrapper').addClass('error');
		$('#manSkills .dropdown-wrapper').html('Please enter your skills');
		$('html, body').animate({
					scrollTop: $('#skills').offset().top-200
				});
		
	}else{
		$('#manSkills .select2-selection--multiple').removeClass('error');
		$('#manSkills .dropdown-wrapper').html('');
	}
		
	
	var check = formValidations('jobSection');

	if((check) && skills>=1){
	$('#qualSection').show();
	$('#jobSection').hide();
	$('#step4').addClass('formFilled');
	$('#step3').removeClass('formFilled');
	$('.progress-bar').css('width','58%');
	 $('html, body').animate({
			scrollTop: $("#formProgress").offset().top-200
		}, 1000);
	}else{
		return false;
	}
});
$(".step3Back").click(function () {
	$('#jobSection').hide();
	$('#profileSection').show();
	$('.progress-bar').css('width','20%');
	 $('html, body').animate({
			scrollTop: $("#formProgress").offset().top-200
		}, 1000);
});

//Qualification Section Next click action
$(".step4Next").click(function () {

		$('#orgSection').show();
		$('#qualSection').hide();
		$('#step5').addClass('formFilled');
		$('#step4').removeClass('formFilled');
		$('.progress-bar').css('width','78%');
		 $('html, body').animate({
			scrollTop: $("#formProgress").offset().top-200
		}, 1000);
	
});
$(".step4Back").click(function () {
	$('#qualSection').hide();
	$('#jobSection').show();
	$('.progress-bar').css('width','39%');
	 $('html, body').animate({
			scrollTop: $("#formProgress").offset().top-200
		}, 1000);
});

//Orgnisation Section Next click action
$(".step5Next").click(function () {
	$('#interviewSection').show();
	$('#orgSection').hide();
	$('.progress-bar').css('width','100%');
	$('#step6').addClass('formFilled');
	$('#step5').removeClass('formFilled');
	 $('html, body').animate({
			scrollTop: $("#formProgress").offset().top-200
		}, 1000);
});
$(".step5Back").click(function () {
	$('#orgSection').hide();
	$('#qualSection').show();
	$('.progress-bar').css('width','58%');
	 $('html, body').animate({
			scrollTop: $("#formProgress").offset().top-200
		}, 1000);
});

//Interview Section Next click action
$(".step6Back").click(function () {
	$('#interviewSection').hide();
	$('#orgSection').show();
	$('.progress-bar').css('width','78%');
	 $('html, body').animate({
			scrollTop: $("#formProgress").offset().top-200
		}, 1000);
});


//Clear filter criteria in popup
$('.ClearCross, .filterCross').click(function (e) {
	$('input,textarea').val('');
	$('select').prop("selectedIndex", 0);
    resetFilters([
        "#projectType",
        "#branchType",
        "#employmentType",
        "#companyType",
        "#costBranchType",
        "#departmentType"
    ], "#leavesfilter", "#filterBarLeaves");
}); //End function	


//Remove filter elements
function removeFilter(val, elementType) {
    let fieldValue = $(elementType).children("option:selected").val();
    (fieldValue === val) ? $(elementType).get(0).selectedIndex = 0: null;
    applyReviewApprovalFilters([
        "#projectType",
        "#branchType",
        "#employmentType",
        "#companyType",
        "#costBranchType",
        "#departmentType"
    ], "#leavesfilter", "#filterBarLeaves");
    return false;
}

$("#selectEmployee").click(function(){
    let checkBoxes = $('#employeeTable tbody tr input').toArray();
    let selectedCheckBox = checkBoxes.filter(checkBox => checkBox.checked);
    let employeeCode = selectedCheckBox[0].attributes["data-empcode"].value;
    $('#empSearch').val(employeeCode);

    $('body').css('overflow', 'auto')
  $('.jquery-modal').hide();
  $(".modal").hide();
})


	
	 

