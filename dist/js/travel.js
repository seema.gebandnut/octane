// Radio button show hide div on click
$('#oneWay').click(function() {
	$('#oneWayTravel')[0].style.display = 'block';
	$('#roundTriptravel')[0].style.display = 'none';
	$('#multiCityTravel')[0].style.display = 'none';
});

$('#roundTrip').click(function() {
	$('#oneWayTravel')[0].style.display = 'none';
	$('#roundTriptravel')[0].style.display = 'block';
	$('#multiCityTravel')[0].style.display = 'none';
});

// Radio button show hide div on click
$('#multiCity').click(function() {
	$('#oneWayTravel')[0].style.display = 'none';
	$('#roundTriptravel')[0].style.display = 'none';
	$('#multiCityTravel')[0].style.display = 'block';
});

function styleEl(clickEl,styleEl,value){
	$(clickEl).click(function() {
		$(styleEl)[0].style.display = value;
	});
}


// Radio button show hide div on click
styleEl('#ticketRequiredYes','#hideTicketRequired','flex');
styleEl('#ticketRequiredNo','#hideTicketRequired','none');

// Radio button show hide div on click
styleEl('#hotelRequiredYes','#hideHotelRequired','block');
styleEl('#hotelRequiredNo','#hideHotelRequired','none');

// Radio button show hide div on click
styleEl('#advancedRequiredYes','#hideAdvancedRequired','flex');
styleEl('#advancedRequiredNo','#hideAdvancedRequired','none');
// Radio button show hide div on click
styleEl('#advancedRequiredNo','#hideAdvancedRequireds','flex');
styleEl('#advancedRequiredYes','#hideAdvancedRequireds','none');



// Radio button show hide div on click
styleEl('#raoundTripticketRequiredYes','#roundTripticketRequired','block');
styleEl('#raoundTripticketRequiredNo','#roundTripticketRequired','none');

// Radio button show hide div on click
styleEl('#roundTripHotelRequiredYes','#roundTripHotelRequired','block');
styleEl('#roundTripHotelRequiredNo','#roundTripHotelRequired','none');

// Radio button show hide div on click
styleEl('#roundTripAdvancedRequiredYes','#hideroundTripAdvancedRequired','flex');
styleEl('#roundTripAdvancedRequiredNo','#hideroundTripAdvancedRequired','none');

// Radio button show hide div on click
styleEl('#multiTicketRequiredYes','#hidemultiTicketRequired','flex');
styleEl('#multiTicketRequiredNo','#hidemultiTicketRequired','none');

// Radio button show hide div on click
styleEl('#MultiHotelRequiredYes','#hideMultiHotelRequired','block');
styleEl('#MultiHotelRequiredNo','#hideMultiHotelRequired','none');

// Radio button show hide div on click
styleEl('#MultiAdvancedRequiredYes','#hideMultiAdvancedRequired','flex');
styleEl('#MultiAdvancedRequiredNo','#hideMultiAdvancedRequired','none');

//Add more location
jQuery("#addMore").click(function () {
	
	var liLen = $('.nav-tabs li.tabli').length;
	var nextLi = liLen+1;
	
	if(nextLi==5){
		jQuery("#addMore").hide();
	}
	jQuery('li.tabli a').removeClass('active');
	jQuery('span.crossIcon').remove();
	jQuery(this).before('<li class="nav-item tabli" id="tab-'+nextLi+'"><a class="nav-link active" data-toggle="tab" href="#Location'+nextLi+'">Location '+nextLi+'<span class="crossIcon" onclick="removeLocation('+nextLi+')"><img src="../dist/images/crossBlack.png"></span></a></li>');
	jQuery('.multicity').hide();
	jQuery('#Location'+nextLi).show();

});

//Remove Location
function removeLocation(id) {

	jQuery("#tab-"+id).remove();
	jQuery("#Location"+id).hide();
		
	var liLen = $('.nav-tabs li.tabli').length;
	
	var prevTab = liLen;
	
	if(liLen<5){
		jQuery("#addMore").show();
	}
	
	jQuery('#tab-'+prevTab+' a').addClass('active');
	jQuery('#tab-'+prevTab+' a').append('<span class="crossIcon" onclick="removeLocation('+prevTab+')"><img src="../dist/images/crossBlack.png"></span>')
	jQuery("#Location"+liLen).show();
}





