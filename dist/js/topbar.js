/*
Left side bar menu file
*/

$('nav').html(` <ul class='m-0 p-0'>
<li>
    <form role='search' class='search-form'>
        <label class='m-0 p-0'>
            <input type='search' class='search-field' placeholder='Search' value='' name='s' title='search' autocomplete='off'> </label>
    </form>
</li>
<li style='position: relative; bottom: -4px;'>
    <a href='#' class='togglePopup' id='toggleNotificationPopup'> <svg width="19" height="21" viewBox="0 0 19 21" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M10.8534 18.4458C10.6907 18.7262 10.4572 18.959 10.1763 19.1208C9.89536 19.2826 9.57685 19.3678 9.25266 19.3678C8.92847 19.3678 8.60997 19.2826 8.32904 19.1208C8.04812 18.959 7.81463 18.7262 7.65196 18.4458M18.5053 14.7447H6.10352e-05C0.736244 14.7447 1.44227 14.4523 1.96283 13.9317C2.48339 13.4112 2.77584 12.7051 2.77584 11.969V7.34267C2.77584 5.62491 3.45822 3.9775 4.67286 2.76286C5.8875 1.54822 7.5349 0.865845 9.25266 0.865845C10.9704 0.865845 12.6178 1.54822 13.8325 2.76286C15.0471 3.9775 15.7295 5.62491 15.7295 7.34267V11.969C15.7295 12.7051 16.0219 13.4112 16.5425 13.9317C17.0631 14.4523 17.7691 14.7447 18.5053 14.7447V14.7447Z" stroke="#869094" stroke-width="1.33333" stroke-linecap="round"/>
    </svg> <span class='notificationNumber'>2</span> </a>
    <div class='popup' id='notification'>
        <div class='popupHeader'>
       <img src='../dist/images/Arrow1.png' alt='arrowUp'>
            <h5>Notifications</h5>
            <h5>Filter</h5> </div>
        <div class='popupBody'>
            <p class='mt-2'> TODAY</p>
            <div class='notificationArea'>
            <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'> <rect x='6' y='3' width='1' height='3' fill='#888'></rect> <rect x='4' y='8' width='15' height='1' fill='#888'></rect> <rect x='11' y='3' width='1' height='3' fill='#888'></rect> <rect x='11' y='10' width='1' height='6' fill='#888'></rect> <rect x='11' y='17' width='1' height='1' fill='#888'></rect> <rect x='16' y='3' width='1' height='3' fill='#888'></rect> <rect x='4.5' y='5.5' width='14' height='15' rx='1.5' stroke='#888'></rect> </svg>
                <p> <a href='#'> Sachin Shah</a> requested for 3 days leave starting from Feb 23. </p> <span>3 mins ago</span> </div>
            <div class='notificationArea mt-3'> 
            <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'> <path d='M10.7577 20.3847C9.97662 21.1658 8.71029 21.1658 7.92924 20.3847C7.14819 19.6037 7.14819 18.3373 7.92924 17.5563C8.71029 16.7752 9.97662 16.7752 10.7577 17.5563C11.5387 18.3373 11.5387 19.6037 10.7577 20.3847Z' fill='#888'></path> <path d='M12.8787 6.94968L15.7071 9.77811C16.0976 10.1686 16.0976 10.8018 15.7071 11.1923L10.6367 16.2628C10.936 16.4058 11.2165 16.6012 11.4645 16.8492L16.4142 11.8994C17.1953 11.1184 17.1953 9.85205 16.4142 9.071L13.5858 6.24257C12.8047 5.46153 11.5384 5.46153 10.7574 6.24257L4.3934 12.6065C3.61236 13.3876 3.61235 14.6539 4.3934 15.435L6.63543 17.677C6.77843 17.3777 6.97389 17.0971 7.22183 16.8492L5.10051 14.7279C4.70999 14.3373 4.70999 13.7042 5.10051 13.3136L11.4645 6.94968C11.855 6.55916 12.4882 6.55916 12.8787 6.94968Z' fill='#888'></path> <path d='M14.2932 8.36389L13.5861 7.65679L5.10077 16.1421L5.80788 16.8492L14.2932 8.36389Z' fill='#888'></path> <path d='M20.3034 8.01034C20.4986 7.81508 20.4986 7.4985 20.3034 7.30324C20.1081 7.10797 19.7915 7.10797 19.5963 7.30324L11.8181 15.0814C11.6228 15.2767 11.6228 15.5933 11.8181 15.7885C12.0134 15.9838 12.3299 15.9838 12.5252 15.7885L20.3034 8.01034Z' fill='#888'></path> <path d='M20.3034 8.01034C20.4986 7.81508 20.4986 7.4985 20.3034 7.30324L18.8892 5.88902C18.6939 5.69376 18.3773 5.69376 18.1821 5.88902C17.9868 6.08428 17.9868 6.40087 18.1821 6.59613L19.5963 8.01034C19.7915 8.2056 20.1081 8.2056 20.3034 8.01034Z' fill='#888'></path> </svg>
                <p> <a href='#'> Suhana Mehta </a> Requested for one way travel from Delhi to Mumbai.</p>
                </p> <span>1 hour ago</span> </div>
            <p class='mt-4'> YESTERDAY</p>
            <div class='notificationArea'> 
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"> <path fill-rule="evenodd" clip-rule="evenodd" d="M15 11.0189V4C15 3.44772 14.5523 3 14 3H9.5H5C4.44772 3 4 3.44772 4 4V18C4 18.5523 4.44772 19 5 19H8.17393C8.25537 19.3448 8.36426 19.6789 8.49816 20H5C3.89543 20 3 19.1046 3 18V4C3 2.89543 3.89543 2 5 2H9.5H14C15.1046 2 16 2.89543 16 4V11.1739C15.6754 11.0973 15.3414 11.0449 15 11.0189Z" fill="#888"></path> <circle cx="14.5" cy="17.5" r="5" stroke="#888"></circle> <rect x="5" y="5" width="8" height="1" fill="#888"></rect> <rect x="5" y="7" width="3" height="1" fill="#888"></rect> <rect x="5" y="9" width="6" height="1" fill="#888"></rect> <rect x="14" y="17" width="3" height="1" fill="#888"></rect> <rect x="14" y="17" width="3" height="1" transform="rotate(-90 14 17)" fill="#888"></rect> </svg>
                <p><a href='#'> Ayesha Mehta</a> submitted the timesheet for the week Feb 21 - Feb 27.</p> <span>6:48 pm</span> </div>
        </div>
    </div>
</li>

<li style='position: relative;'>
    <a href='#' class='togglePopup' id='toggleProfile'> <img src='../dist/images/manoj.png' alt='user' /> <i class="fa fa-chevron-down ml-2"></i> </a>
    <div class='popup' id='userProfile'>
    <img src='../dist/images/Arrow1.png' alt='arrowUp'>
        <ul>
            <li><img src='../dist/images/viewProfile.png' alt='profile' /> View Profile</li>
            <li class='underLine'></li> <span>ROLES</span>
            <li><svg width="14" height="16" viewBox="0 0 14 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M7 0L0 2.66667V5.8426C0 10.3528 2.78529 14.3944 7 16C11.2147 14.3944 14 10.3528 14 5.8426V2.66667L7 0ZM7 1L1 3.33333V6.22893C1 10.1101 3.38269 13.5933 7 15C10.6173 13.5933 13 10.1101 13 6.22893V3.33333L7 1Z" fill="#869094"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M11.7129 12.2869C10.6141 10.8941 8.91124 10 6.99952 10C5.14086 10 3.47959 10.8451 2.37903 12.1721L2.99959 12.9994C3.91182 11.7853 5.36396 11 6.99952 11C8.63531 11 10.0876 11.7855 10.9998 12.9999L11.7129 12.2869Z" fill="#869094"/>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M7 8C8.10457 8 9 7.10457 9 6C9 4.89543 8.10457 4 7 4C5.89543 4 5 4.89543 5 6C5 7.10457 5.89543 8 7 8ZM7 7C7.55228 7 8 6.55228 8 6C8 5.44772 7.55228 5 7 5C6.44772 5 6 5.44772 6 6C6 6.55228 6.44772 7 7 7Z" fill="#869094"/>
            </svg>
             Admin View </li>
            <li><img src='../dist/images/hr-view.png' alt='hr' /> HR View </li>
            <li class='underLine'></li>
            <li><svg width="12" height="14" viewBox="0 0 12 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M9.00005 9V8H4.00005V6H9L9.00005 5L12 7L9.00005 9ZM8.00005 0.500049V0.500049C8.55233 0.500049 9.00005 0.947768 9.00005 1.50005V1.50005V3.50008H8.00005V1.50016H1.00005V12.5002H8.00005V10.5002L9 10.5V12.5V12.5C9 13.0523 8.55229 13.5 8 13.5V13.5H1V13.5C0.447818 13.5 4.86374e-05 13.0523 4.86374e-05 12.5002V12.5002V1.50005V1.50005C2.17766e-05 0.947757 0.447753 0.500024 1.00005 0.500053V0.500053L8.00005 0.500049Z" fill="#d62020"/>
            </svg> Logout</li>
        </ul>
    </div>
</li>
</ul> `);