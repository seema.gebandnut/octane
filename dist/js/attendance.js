
// if all checkbox are selected, check the selectall checkbox  also        
$(".case").click(function () {
  let checkboxLen = $('.case:checked').length;
  if ($(".case").length == checkboxLen) {

    $("#selectall").prop("checked", true);
  } else {

    $("#selectall").prop("checked", false);
  }


});

// Start multiple select / deselect functionality
$("#selectall").click(function () {
  $('.case').prop('checked', this.checked);
  let checkboxLen = $('.case:checked').length;
  if (checkboxLen >= 1) {
      $('#approve,#reject').removeAttr('disabled');
  }
  if (checkboxLen == 0) {
      $('#approve,#reject').attr('disabled', 'disabled');
  }
});

function onCheck() {
  let checkboxLen = $('.case:checked').length;

  if (checkboxLen == 0) {
    $('#approve,#reject').attr('disabled', 'disabled');

  }
  if (checkboxLen == 1) {
    $('#approve,#reject').removeAttr('disabled');
  }

}