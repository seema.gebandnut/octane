// JavaScript file for common functions 

(function ($) {


  //Close topbar popups on window click

  $(document).mouseup(function (e) {

    var notificationCon = $("#notification");

    if (!notificationCon.is(e.target) && notificationCon.has(e.target).length === 0) {

      notificationCon.fadeOut();
      $("body").enableScroll();

    }

    var userProfileCon = $("#userProfile");

    if (!userProfileCon.is(e.target) && userProfileCon.has(e.target).length === 0) {

      userProfileCon.fadeOut();
      $("body").enableScroll();

    }

    var userProfileCon = $("#toggledivSidebar");

    if (!userProfileCon.is(e.target) && userProfileCon.has(e.target).length === 0) {

      userProfileCon.fadeOut();
      $("body").enableScroll();

    }


    var userProfileCon = $("#leavesCardContent");

    if (!userProfileCon.is(e.target) && userProfileCon.has(e.target).length === 0) {

      userProfileCon.fadeOut();
      $("body").enableScroll();

    }

    var userProfileCon = $("#attendanceCardContent");

    if (!userProfileCon.is(e.target) && userProfileCon.has(e.target).length === 0) {

      userProfileCon.fadeOut();
      $("body").enableScroll();

    }

    var userProfileCon = $("#assetCardContent");

    if (!userProfileCon.is(e.target) && userProfileCon.has(e.target).length === 0) {

      userProfileCon.fadeOut();
      $("body").enableScroll();

    }
    var userProfileCon = $("#fullFinalCardContent");

    if (!userProfileCon.is(e.target) && userProfileCon.has(e.target).length === 0) {

      userProfileCon.fadeOut();
      $("body").enableScroll();

    }
    var userProfileCon = $("#leavesCardContent");

    if (!userProfileCon.is(e.target) && userProfileCon.has(e.target).length === 0) {

      userProfileCon.fadeOut();
      $("body").enableScroll();

    }

    var userProfileCon = $("#actionsCardContent");

    if (!userProfileCon.is(e.target) && userProfileCon.has(e.target).length === 0) {

      userProfileCon.fadeOut();
      $("body").enableScroll();

    }

    var userProfileCon = $("#profileCardContent");

    if (!userProfileCon.is(e.target) && userProfileCon.has(e.target).length === 0) {

      userProfileCon.fadeOut();
      $("body").enableScroll();

    }

    var userProfileCon = $("#quickCardContent");

    if (!userProfileCon.is(e.target) && userProfileCon.has(e.target).length === 0) {

      userProfileCon.fadeOut();
      $("body").enableScroll();

    }

    var userProfileCon = $("#reimbursementCardContent");

    if (!userProfileCon.is(e.target) && userProfileCon.has(e.target).length === 0) {

      userProfileCon.fadeOut();
      $("body").enableScroll();

    }
  });


  $('#toggleNavBar').click(function () {
	  
	  $('.sidebar').find("ul").slideUp();
	  
	  
    let leftSidebar = $('.leftSidebar');
    if (leftSidebar.hasClass('expandedNav')) {
	
		$('#toggleNavBar').attr('title','Expand Menu');
      leftSidebar.animate({
        'width': '64px'
      });
      $('.navbar-header').css({
        'width': 'calc(100% - 64px)'
      });
      $('.headerTop').css({
        'width': 'calc(100% - 64px)'
      });
      leftSidebar.removeClass('expandedNav');
      leftSidebar.addClass('expandedNavClick');
      $(this).css({
        'transform': 'rotate(180deg)'
      });
      $('body').css({
        'padding-left': '64px'
      })
      $('#brand-logo').hide();
      $(".sidebar a>span").addClass('hideNavLinks');
      $('.activeLink').css({
        'width': '48px'
      });
      return;
    }else{
		$('#toggleNavBar').attr('title','C Menu');
	}

    $('body').css({
      'padding-left': '226px'
    })
    leftSidebar.animate({
      'width': '226px'
    });
    leftSidebar.addClass('expandedNav');
    leftSidebar.removeClass('expandedNavClick');
    $(".sidebar a>span").removeClass('hideNavLinks');
    $('#brand-logo').show();
    $(this).css({
      'transform': 'rotate(0)'
    });
    $('.navbar-header').css({
      'width': 'calc(100% - 226px)'
    });
    $('.headerTop').css({
      'width': 'calc(100% - 226px)'
    });
    $('.activeLink').css({
      'width': '215px'
    });
  });

  $('ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');

  $('ul.tabs li a').click(function (g) {
    var tab = $('.tab'),
      index = $(this).closest('li').index();

    $('ul.tabs > li').removeClass('current');
    $(this).closest('li').addClass('current');

    tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp(
      0);
    tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown(0);

    g.preventDefault();
  });


  $('.tableExpand').click(function () {
    $(this).toggleClass('expand').parent().nextUntil('tr.header').slideToggle(100);
    $(this).toggleClass('open');
  });
  var availableTags = [
    "ActionScript",
    "AppleScript",
    "Asp",
    "BASIC",
    "C",
    "C++",
    "Clojure",
    "COBOL",
    "ColdFusion",
    "Erlang",
    "Fortran",
    "Groovy",
    "Haskell",
    "Java",
    "JavaScript",
    "Lisp",
    "Perl",
    "PHP",
    "Python",
    "Ruby",
    "Scala",
    "Scheme"
  ];
  $("#searchFilter").autocomplete({
    source: availableTags
  });

})(jQuery);





// When the user clicks on profile div, open the popup
$('#toggleProfile').click(function () {

  $('#userProfile').show();
  $('#notification').hide();
  $("body").disableScroll();
});

// When the user clicks on notification div, open the popup
$('#toggleNotificationPopup').click(function () {
  $('#notification').show();
  $('#userProfile').hide();
  $("body").disableScroll();
});

$('#toggleRightSidebar').click(function () {

  $('#toggledivSidebar').show();
  //disable
  $("body").disableScroll();

});

/***** Disable or enable scroll top when open div ******/
$.fn.disableScroll = function () {
  window.oldScrollPos = $(window).scrollTop();

  $(window).on('scroll.scrolldisabler', function (event) {
    $(window).scrollTop(window.oldScrollPos);
    event.preventDefault();
  });
};
$.fn.enableScroll = function () {
  $(window).off('scroll.scrolldisabler');
};


// Clear the ratings when clear button is clicked in feedback popup
$("input[name='clearFeedback']").on('click', function () {
  $('.star').removeClass('selected');
})

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars li').on('mouseover', function () {
  let onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

  // Now highlight all the stars that's not after the current hovered star
  $(this).parent().children('li.star').each(function (e) {
    (e < onStar) ? $(this).addClass('hover'): $(this).removeClass('hover');
  });

}).on('mouseout', function () {
  $(this).parent().children('li.star').each(function (e) {
    $(this).removeClass('hover');
  });
});

/* 2. Action to perform on click */
$('#stars li').on('click', function () {
  let onStar = parseInt($(this).data('value'), 10); // Total ratings count in integers
  let stars = $(this).parent().children('li.star');

  for (let star of stars) {
    $(star).removeClass('selected');
  }
  for (i = 0; i < onStar; i++) {
    $(stars[i]).addClass('selected');
  }
});


// Search thorugh table
let search = document.getElementById("searchTable");
if (search) {
	
  search.addEventListener("keyup", function (e) {
    let value = e.target.value.toLowerCase();
    $("tbody tr").filter(function () {
      $(this).toggle(
        $(this)
        .text()
        .toLowerCase()
        .indexOf(value) > -1
      );
    });
  });
  $('#searchTable').click(function (e) {
	 let value = '';
	
    $("tbody tr").filter(function () {
      $(this).toggle(
        $(this)
        .text()
        .toLowerCase()
        .indexOf(value) > -1
      );
    });
	  
  });
}

// Start multiple select / deselect functionality
$("#selectall").click(function () {
  $('.case').prop('checked', this.checked);
  let checkboxLen = $('.case:checked').length;
  if (checkboxLen >= 1) {
    $('#approve,#reject').removeAttr('disabled');
  }
  if (checkboxLen == 0) {
    $('#approve,#reject').attr('disabled', 'disabled');
  }
});


function resetFilters(filterFields, popUpId, filterBarId) {
  for (let field of filterFields) {
      $(field)
          .get(0)
          .selectedIndex = 0;
  }
  var search =
      '<li><a href=' + popUpId + ' rel="modal:open"><label class="btn btn-default"><div>Add Filters</div></label></a></li>';
  $(filterBarId).html(search);
  $('.filterCross').empty();
}

// Apply Learning Filters
function applyReviewApprovalFilters(filterFields, popUpId, filterBarId) {
  let filterValues = [];
  for (let field of filterFields) {
    filterValues.push($(field).children("option:selected").val())
  }



  // Remove filter by clicking cross icon html
  let showFilterButton = (value, fieldType) => {
    return `<li id=${value}>
  <label class="btn btn-default">
    <div>${value}
    <i onclick="removeFilter('${value}','${fieldType}')" class="material-icons">close</i>
    </div>
  </label>
  </li>`;
  }

  let filterSearch = [];
  let getOptionValue = (field) => $(field)
    .children("option:selected")
    .val();
  for (let field of filterFields) {
    (getOptionValue(field) != "null") ?
    filterSearch.push(showFilterButton(getOptionValue(field), field)): null;
  }

  // If there's no filter applied
  if (filterSearch.length == 0) {
    let search = '<li><a href=' + popUpId + ' rel="modal:open"><label class="btn btn-default"><div>Add Filters</div></label></a></li>';
    $('.filterCross').empty();
    $(filterBarId).html(search);
  }

  // If there's more than one filter applied
  if (filterSearch.length >= 1) {
    let search = filterSearch.join('');
    search = search + '<li><a href=' + popUpId + ' rel="modal:open"><label class="btn btn-default"><div>Add Filters</div></label></a></li>';
    $('.filterCross').html('Clear');
    $(filterBarId).html(search);
  }

  // If there's more than three filters applied
  let noOfFiltersToShow = 3;
  if (filterSearch.length > noOfFiltersToShow) {
    let restLen = filterSearch.length - noOfFiltersToShow;
    let search = filterSearch.slice(0, noOfFiltersToShow).join('');
    search = search + '<li><a href=' + popUpId + ' rel="modal:open"><label class="btn btn-default"><div>' + restLen + ' more</div></label></a></li>';
    $('.filterCross').html('Clear');
    $(filterBarId).html(search);
  }

  $('body').css('overflow', 'auto')
  $('.jquery-modal').hide();
  $(".modal").hide();
}
if($(".tableSort").toArray().length !== 0){
  $(".tableSort").tablesorter();
}

// JSON to CSV
let convertToCSV = objArray => {
  var array =
    typeof objArray !== "object" ? JSON.parse(objArray) : objArray;
  var str = "";

  for (var i = 0; i < array.length; i++) {
    var line = "";
    for (var index in array[i]) {
      if (line !== "") line += ",";

      line += array[i][index];
    }

    str += line + "\r\n";
  }

  return str;
};



let exportCSVFile = (headers, items, fileTitle) => {
  if (headers) {
    items.unshift(headers);
  }

  // Convert Object to JSON
  var jsonObject = JSON.stringify(items);

  var csv = convertToCSV(jsonObject);

  var exportedFilenmae = fileTitle + ".csv" || "export.csv";

  var blob = new Blob([csv], {
    type: "text/csv;charset=utf-8;"
  });
  if (navigator.msSaveBlob) {
    // IE 10+
    navigator.msSaveBlob(blob, exportedFilenmae);
  } else {
    var link = document.createElement("a");
    if (link.download !== undefined) {
      var url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", exportedFilenmae);
      link.style.visibility = "hidden";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
};


//toggle tooltip
$(document).ready(function () {
  $('[data-toggle="tooltip"]').tooltip();
});


/* Updated On 4/22/2019 by Seema */
/* Updated on 17/4/2018 by Seema */
//Start Form Validations
function formValidations(div,popUp=null) {
	$("input, textarea,select").removeClass('error');
    $("label.error").remove();

   var check = true;
  $('#' + div).find(".mandatory").each(function (e) {

	  var idVal = this.id; //get field id

	  if( $('#' + idVal).is(":visible")){ //check field visibility

		var visible = true;
	  }else{
		var visible = false;
	  }

    if ($('#' + idVal).val() == '' && visible==true) {

      var textVal = $(this).parent().find('.labelGroup').text(); //get field label text
	  textVal = textVal.replace("'", "");
      msg = textVal.replace('*', ''); //get msg value
	 
	  if(msg==''){
		msg = "This field";
	  }

	  $('#' + idVal + '-error').remove();
      $('#' + idVal).addClass('error');
      $('#' + idVal).after('<label id="' + idVal + '-error" class="error">' + msg + ' should not be blank</error>');
      check = false;

    }else{

		 if ($('#' + idVal).val() != '') {
			$('#' + idVal).removeClass('error');
			$('#' + idVal + '-error').remove();
			check = true;
		 }
	} 

  });
 

  $('#' + div).find(".mobile").each(function (e) { // mobile validations
    var idVal = this.id;
	
    if ($('#' + idVal).val() != '') {
      var len = $(this).val().length;
      if (len < 10) {
		$('#' + idVal + '-error').remove();
        $('#' + idVal).addClass('error');
        $('#' + idVal).after('<label id="' + idVal + '-error" class="error"> This is not valid number</error>');
	   check = false;
      }
    }
	
  });

 
  if (check == true) { //Updated for success messages on 20/6/2019
	  
		$('.validationMessage').remove();
	  
		//form submit event
		if(popUp=='yes'){
			$( ".popup-header" ).after( '<div class="validationMessage"><div class="successMessage"><i class="material-icons">done</i>You’ve succesfully saved information.</div></div>' );
			setTimeout(function(){ jQuery(".validationMessage").hide();},3000);
			
		}else{
			$( ".content" ).before( '<div class="validationMessage successMessage mgtop5"><i class="material-icons">done</i>You’ve succesfully saved information.</div>' );
			/*$('html, body').animate({
				scrollTop: $('.validationMessage').top-100});*/
			setTimeout(function(){ jQuery(".validationMessage").hide();},3000);
			
			 return check;

		}
  }else{
	  return check;

  }


} //End Function



//Reset Form 
function clearForm(div) {

   $('#' + div).find("input[type=text], textarea,select").val("");
   $('#' + div).find("input[type=text], textarea,select").removeClass('error');
   $("label.error").remove();
} //End Function



//Reset Form 
function clearpopupForm(div) {

   $('#' + div).find("input[type=text], textarea,select").val("");
   $('#' + div).find("input[type=text], textarea,select").removeClass('error');
    $("label.error").remove();
} //End Function



//Required field change function for validation
$(".mandatory").change(function () {
  var idVal = this.id;

  if ($('#' + idVal).val() != '') {
    $('#' + idVal).removeClass('error');
    $('#' + idVal + '-error').remove();
  }
});


//Mobile field change function for validation
$(".mobile").change(function () {
  var idVal = this.id;
  if ($('#' + idVal).val() != '') {
    var len = $(this).val().length;

    if (len < 10) {
		
      $('#' + idVal).addClass('error');
      $('#' + idVal).after('<label id="' + idVal + '-error" class="error"> This is not valid number</error>');
		
    }
  }
});

/* Updated on 6/1/2019 by Seema */
  $(".decimal").keydown(function (event) {


		if (event.shiftKey == true) {
			event.preventDefault();
		}

		if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

		} else {
			event.preventDefault();
		}
		
		if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
			event.preventDefault();
 });

//mobile change funtion
$('.mobile').keyup(function (e) {
  if (/\D/g.test(this.value)) {
    // Filter non-digits from input value.
    this.value = this.value.replace(/\D/g, '');
  }
});
//End Form Validations		

//Left sidebar menu scripts 
$("#accordian a").click(function() {
	var link = $(this);
	var closest_ul = link.closest("ul");
	var parallel_active_links = closest_ul.find(".active")
	var closest_li = link.closest("li");
	var link_status = closest_li.hasClass("active");

	if(link_status==true){

		$(this).find('i').removeClass('fa-sort-down');
		$(this).find('i').addClass('fa-sort-up');
	}else{

		$(this).find('i').removeClass('fa-sort-up');
		$(this).find('i').addClass('fa-sort-down');
	}

	var count = 0;

	closest_ul.find("ul").slideUp(function() {
		if (++count == closest_ul.find("ul").length)
			parallel_active_links.removeClass("active");
	});

	if (!link_status) {

		closest_li.children("ul").slideDown();
		closest_li.addClass("active");
	}
});
	
//Clear filter criteria
$('.ClearCross, .filterCross,.clearTableFilters,.clearButton ').click(function (e) {
	$('.jquery-modal').find('input[type="text"],textarea').val('');
	$('.jquery-modal').find('select').prop("selectedIndex", 0);
	$('.jquery-modal').find("input[type='checkbox']"). prop("checked", false);

}); //End function

/* Start table pagination */
 $('table.paginated').each(function() {
        var currentPage = 0;
        var numPerPage  = 10;
        var $table = $(this);
        $table.bind('repaginate', function() {
            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr').length;
        var numPages = Math.ceil(numRows / numPerPage);
        var $pager = $('<div class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function(event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertAfter('.actionBtns').find('span.page-number:first').addClass('active');
    });
    /** End Pagination on table **/
	
	
//filter Apply Button click function	
$("#applyBtn").on('click', function () {

	 $(".jquery-modal,.ui-dialog").hide();
	
});

//Apply filter data after selecting employee from popup
$("#selectEmployee").click(function(){
	let checkBoxes = $('#employeeTable tbody tr input').toArray();
	let selectedCheckBox = checkBoxes.filter(checkBox => checkBox.checked);
	let employeeCode = selectedCheckBox[0].attributes["data-empcode"].value;
	$('#empSearch').val(employeeCode);
	$('body').css('overflow', 'auto')
	$('.jquery-modal').hide();
	$(".modal").hide();
	$(".ui-dialog").hide();
});


// Populate Table
let table = $("#leavesTable tbody")[0];

var headers = {
 reqNo: "Request No",
 type: "Type",
 fromDate: "From",
 toDate: "To",
 reason: "Reason",
 approval: "Approval",
 status: "Status",
 project: "Project",
 branch: "Branch",
 employmentType: "Employment Type",
 company: "Company",
 costBranch: "Cost Branch",
 department: "Department"
};

var itemsFormatted = [];

$(".downloadCSV").on("click", function () {
 exportCSVFile(headers, itemsFormatted, "My Leaves History");
});


//Close popup function
$(document).on("click", "a.close-modal", function () {
  alert('Closed window successfully!');
});

