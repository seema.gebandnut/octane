// Initialize popup
$("#attachmentpopupcard").dialog({
    autoOpen: false,
    modal: true,
    closeOnEscape: true
});
$("#uploadFilePopup").dialog({
    autoOpen: false,
    modal: true,
    closeOnEscape: true
});

 // Open popup on click
 $(document).on("click", "#attachmentpopupOpen", function () {
    popUp("#attachmentpopupcard", "open");
    $("#attachmentpopupcard").css({
        visibility: "visible"
    });
});
 $(document).on("click", "#uploadFile", function () {
    popUp("#uploadFilePopup", "open");
    $("#uploadFilePopup").css({
        visibility: "visible"
    });
});
 // Close popup on click
 $("#closePopup, #closePopupBtn, #applyCardsOrder").on("click", function () {
    popUp("#attachmentpopupcard", "close");
});
 $("#closePopup, #closePopupBtn, #applyCardsOrder").on("click", function () {
    popUp("#uploadFilePopup", "close");
});

  // Function for closing popup
  function popUp(element, action) {
    $(element).dialog(action);
    return false;
}
//Clear filter criteria in popup
$('.ClearCross, .filterCross').click(function (e) {
	$('input,textarea').val('');
	$('select').prop("selectedIndex", 0);
    resetFilters([
        "#projectType",
        "#branchType",
        "#employmentType",
        "#companyType",
        "#costBranchType",
        "#departmentType"
    ], "#leavesfilter", "#filterBarLeaves");
}); //End function	