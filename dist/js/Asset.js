﻿/** Start Pagination on table **/
$(document).ready(function(){
    $('table.paginated').each(function() {
        var currentPage = 0;
        var numPerPage  = 10;
        var $table = $(this);
        $table.bind('repaginate', function() {
            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr').length;
        var numPages = Math.ceil(numRows / numPerPage);
        var $pager = $('<div class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function(event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertAfter('.actionBtns').find('span.page-number:first').addClass('active');
    });
    /** End Pagination on table **/

    $("#selectall").click(function () {
        $('.case').find('input[type="checkbox"]').prop('checked', this.checked);
        let checkboxLen = $('input[type="checkbox"]:checked').length;
        if (checkboxLen >= 1) {
            $('#approve,#reject').removeAttr('disabled');
        }
        if (checkboxLen == 0) {
            $('#approve,#reject').attr('disabled', 'disabled');
        }
    });

    $(".case").find('input[type="checkbox"]').click(function () {
        let checkboxLen =  $(".case").find('input[type="checkbox"]:checked').length;
        if ($(".case").find('input[type="checkbox"]').length == checkboxLen) {
            $("#selectall").prop("checked", true);
        } else {
            $("#selectall").prop("checked", false);
        }
    });


    $('#approveBtn').click(function (e) {
        $('body').css('overflow', 'auto');
        $('.jquery-modal').hide();
        $(".modal").hide();

    }); 

    $('.filterCross').click(function (e) {
        table.innerHTML = tableHtml(tableData);
        resetTable();
    }); 

    function applyReviewApprovalFilters() {
        let filterValues = [];
        let projectValue = $("#projectType").children("option:selected").val();
        filterValues.push(projectValue);
        let branchValue = $("#branchType").children("option:selected").val();
        filterValues.push(branchValue);
        let empValue = $("#employmentType").children("option:selected").val();
        filterValues.push(empValue);
        let companyValue = $("#companyType").children("option:selected").val();
        filterValues.push(companyValue);
        let costBranchValue = $("#costBranchType").children("option:selected").val();
        filterValues.push(costBranchValue);
        let departmentValue = $("#departmentType").children("option:selected").val();
        filterValues.push(departmentValue);


        var filterSearch = [];

        // Remove filter by clicking cross icon html
        let showFilterButton = (value, fieldType) => {
            return `<li id=${value}>
            <label class="btn btn-default">
              <div>${value}
    <i onclick="removeFilter('${value}','${fieldType}')" class="material-icons">close</i>
    </div>
  </label>
  </li>`;
}
    
(projectValue != "null") ? filterSearch.push(showFilterButton(projectValue, "#projectType")) : null;
(empValue != "null") ? filterSearch.push(showFilterButton(empValue, "#employmentType")) : null;
(costBranchValue != "null") ? filterSearch.push(showFilterButton(costBranchValue, "#costBranchType")) : null;
(branchValue != "null") ? filterSearch.push(showFilterButton(branchValue, "#branchType")) : null;
(companyValue != "null") ? filterSearch.push(showFilterButton(companyValue, "#companyType")) : null;
(departmentValue != "null") ? filterSearch.push(showFilterButton(departmentValue, "#departmentType")) : null;

var allFilterlen = filterSearch.length;
var search = filterSearch;
    

if (allFilterlen == 0) {
    search = `<li><a href="#leavesfilter" rel="modal:open"><label class="btn btn-default"><div>Add Filters</div></label></a></li>`;
    $('.filterCross').empty();
    $('#filterBarLeaves').html(search);
}
if (allFilterlen >= 1) {
    let search = filterSearch.join('');
    search = `${search} <li><a href="#leavesfilter" rel="modal:open"><label class="btn btn-default"><div>Add Filters</div></label></a></li>`;
    $('.filterCross').html('Clear');
    $('#filterBarLeaves').html(search);
}
if (allFilterlen > 3) {
    let restLen = allFilterlen - 3;
    let search = filterSearch.slice(0, 3).join('');
    search = `${search} <li><a href="#leavesfilter" rel="modal:open"><label class="btn btn-default"><div>${restLen} more</div></label></a></li>`;
    $('.filterCross').html('Clear');
    $('#filterBarLeaves').html(search);
}

$('body').css('overflow', 'auto')
$('.jquery-modal').hide();
$(".modal").hide();

if (!filterValues.every(i => i === "null")) {
    let newTable = tableData
        .filter(row => row.project.toLowerCase().includes(filterValues[0] !== "null" ? filterValues[0] : ''))
        .filter(row => row.branch.toLowerCase().includes(filterValues[1] !== "null" ? filterValues[1] : ''))
        .filter(row => row.employmentType.toLowerCase().includes(filterValues[2] !== "null" ? filterValues[2] : ''))
        .filter(row => row.company.toLowerCase().includes(filterValues[3] !== "null" ? filterValues[3] : ''))
        .filter(row => row.costBranch.toLowerCase().includes(filterValues[4] !== "null" ? filterValues[4] : ''))
        .filter(row => row.department.toLowerCase().includes(filterValues[5] !== "null" ? filterValues[5] : ''))
    filterValues = [];
    table.innerHTML = tableHtml(newTable);
    return;
}
table.innerHTML = tableHtml(tableData);
}
//Clear filter criteria
$('.ClearCross, .filterCross').click(function (e) {
    resetFilters([
        "#projectType",
        "#branchType",
        "#employmentType",
        "#companyType",
        "#costBranchType",
        "#departmentType"
    ], "#leavesfilter", "#filterBarLeaves");
}); //End function
//Remove filter elements
function removeFilter(val, elementType) {
    let fieldValue = $(elementType).children("option:selected").val();
    (fieldValue === val) ? $(elementType).get(0).selectedIndex = 0: null;
    applyReviewApprovalFilters([
        "#projectType",
        "#branchType",
        "#employmentType",
        "#companyType",
        "#costBranchType",
        "#departmentType"
    ], "#leavesfilter", "#filterBarLeaves");
    return false;
}

$("#selectEmployee").click(function(){
    let checkBoxes = $('#employeeTable tbody tr input').toArray();
    let selectedCheckBox = checkBoxes.filter(checkBox => checkBox.checked);
    let employeeCode = selectedCheckBox[0].attributes["data-empcode"].value;
    $('#empSearch').val(employeeCode);

    $('body').css('overflow', 'auto')
    $('.jquery-modal').hide();
    $(".modal").hide();
})
});
