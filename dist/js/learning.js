// Populate Table
if ($("#learningApprovalsTable tbody")[0]) {
    let table = $("#learningApprovalsTable tbody")[0];
}

/** End Table load data and sorting **/



// if all checkbox are selected, check the selectall checkbox  also
if ($(".case")) {
    $(".case")
        .click(function () {
            let checkboxLen = $('.case:checked').length;
            if ($(".case").length == checkboxLen) {
                $("#selectall").prop("checked", true);
            } else {

                $("#selectall").prop("checked", false);
            }
        });
}



function onCheck() {
    let checkboxLen = $('.case:checked').length;
    if (checkboxLen == 0) {
        $('#approve,#reject').attr('disabled', 'disabled');
    }
    if (checkboxLen == 1) {
        $('#approve,#reject').removeAttr('disabled');
    }

}

// approve Popup form submit function
$('#approveBtn')
    .click(function (e) {
        $('body').css('overflow', 'auto');
        $('.jquery-modal').hide();
        $(".modal").hide();

    }); //End function


// Change Sort Icon
$("thead th")
    .not(":eq(0)")
    .on("click", function () {
        /* Updated on 17/4/2018 by Praveen */
        let iconClasses = [];
        if($(this)[0].children.length > 1) {
            iconClasses = $(this)[0].children[1].classList;
        }
        else {
            iconClasses = $(this)[0].children[0].children[1].classList;
        }
        
        if (
            iconClasses.contains("fa-sort") ||
            iconClasses.contains("fa-sort-amount-down")
        ) {
            iconClasses.remove("fa-sort");
            iconClasses.remove("fa-sort-amount-down");
            iconClasses.add("fa-sort-amount-up");
            /* Updated on 17/4/2018 by Praveen */
            $(".tableSort").tablesorter();

            return;
        } else if (iconClasses.contains("fa-sort-amount-up")) {
            iconClasses.remove("fa-sort-amount-up");
            iconClasses.add("fa-sort-amount-down");
            /* Updated on 17/4/2018 by Praveen */
            $(".tableSort").tablesorter();

            return;
        }
    });



(function ($) {
    let headers = {
        reqNo: "Request No.",
        reqBy: "Requested By",
        title: "Title",
        type: "Type",
        fromDate: "From",
        toDate: "To",
        level: "Level",
        cost: "Cost",
        project: "Project",
        branch: "Branch",
        employmentType: "Employment"
    };

    let itemsFormatted = [];

    $(".downloadCSV").on("click", function () {
        exportCSVFile(headers, itemsFormatted, "Approvals");
    });

})(jQuery);